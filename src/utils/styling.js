import { css } from 'styled-components';
import { breakpoints } from './constants';

export const respondTo = Object.keys(breakpoints).reduce(
  (accumulator, label) => {
    accumulator[label] = (...args) => css`
      @media (${label === "phoneOnly" ? "max-width" : "min-width"}: ${breakpoints[label]}) {
        ${css(...args)};
      }
    `;
    return accumulator;
  },
  {}
);

export const searchBarStyle = {
  container: (provided, _) => ({
    ...provided,
    width: "100%",
  }),
  multiValue: styles => {
    return {
      ...styles,
      backgroundColor: 'papayawhip'
    }
  }
};
