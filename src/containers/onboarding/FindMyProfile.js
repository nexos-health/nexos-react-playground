import React, {useState, useEffect} from "react";
import Loading from "../../components/Widgets/Loading";
import styled, {css} from "styled-components/macro";
import {FlexColumn} from "../../components/Widgets/BaseStyledComponents";
import {PromptText, SectionTitle} from "../../components/Widgets/Text";
import {ProfessionalSummaryItem} from "../../components/ProfessionalSummaryItem";
import {useHistory} from "react-router-dom";



export const FindMyProfile = ({...props}) => {
  const history = useHistory();

  let firstName = (props.location.query && props.location.query.firstName) || ""
  let lastName = (props.location.query && props.location.query.lastName) || ""


  let professional = {
    "firstName": "Alexandra",
    "lastName": "Alexandra",
    "title": "Ms",
    "professions": [{
      "uid": "hello",
      "professionType": "Clinical Psychologist"
    }]
  }

  const handleNotFound = () => {
    history.push({
      pathname: "/create-profile",
      query: {"firstName": firstName, "lastName": lastName}
    })
  }

  const handleSelectProfessional = () => {
    history.push({
      pathname: "/create-profile",
      query: {"firstName": firstName, "lastName": lastName}
    })
  }

  return (
    <FindMyProfileContainer>
      <SectionWrapper>
        <TitleContainer>
          <SectionTitle underlined={false}>Select your profile</SectionTitle>
          <PromptText onClick={handleNotFound}>Can't find your name?</PromptText>
        </TitleContainer>
        <ProfilesList>
          <ProfessionalSummaryItem
            key={professional.uid}
            professional={professional}
            handleSelectProfessional={handleSelectProfessional}
          />
          <ProfessionalSummaryItem
            key={professional.uid}
            professional={professional}
            handleSelectProfessional={handleSelectProfessional}
          />
          <ProfessionalSummaryItem
            key={professional.uid}
            professional={professional}
            handleSelectProfessional={handleSelectProfessional}
          />
          <ProfessionalSummaryItem
            key={professional.uid}
            professional={professional}
            handleSelectProfessional={handleSelectProfessional}
          />
        </ProfilesList>
      </SectionWrapper>
    </FindMyProfileContainer>
  )
}


const FindMyProfileContainer = styled(FlexColumn)`
  align-items: center;
  height: 100%;
`;

const SectionWrapper = styled(FlexColumn)`
  width: 100%;
  max-width: 600px;
  align-items: center;
  padding: 100px 25px;
  ${props => props.color && css`
    background-color: ${props.color};  
  `};
  @media (max-width: 800px) {
    padding: 50px 25px;
  }
`;

const TitleContainer = styled.div`
  width: 100%;
  padding: 5px;
  border-bottom: 0.5px solid lightgrey;
`;

const ProfilesList = styled.ul`
  width: 100%;
  text-align: left;
  box-sizing: content-box;
  padding: 0 0.2em;
`;
