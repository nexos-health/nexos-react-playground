import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import { ProfessionalSummary } from '../../components/professional/ProfessionalSummary';
import { InformationContainer } from '../../components/professional/InformationContainer';
import { WaitTimeContainer } from '../../components/professional/WaitTimeContainer';
import { FeesContainer } from '../../components/professional/FeesContainer';
import { LocationContainer } from '../../components/professional/LocationContainer';
import { FlexColumn } from '../../components/Widgets/BaseStyledComponents';
import {
  EditPictureModal,
} from '../../components/Widgets/modals';
import {
  EditAdditionalInfoModal,
  EditFeesModal,
  CreateNewClinicModal,
  EditIntroModal,
  EditWaitTimeModal,
  EditLocationsModal
} from '../../components/Widgets/modals/professional'

export const Professional = () => {
  const [showEditPicModal, setShowEditPicModal] = useState(false);
  const [editPicModalType, setEditPicModalType] = useState('profile');
  const [showEditAdditionalModal, setShowEditAdditionalModal] = useState(false);
  const [showEditFeesModal, setShowEditFeesModal] = useState(false);
  const [showCreateNewClinicModal, setShowCreateNewClinicModal] = useState(false);
  const [showEditIntroModal, setShowEditIntroModal] = useState(false);
  const [showEditWaitTimeModal, setShowEditWaitTimeModal] = useState(false);
  const [showEditLocationsModal, setShowEditLocationsModal] = useState(false);

  useEffect(() => {
    if (showEditPicModal || showEditLocationsModal || showEditAdditionalModal || showEditFeesModal || showCreateNewClinicModal || showEditIntroModal || showEditWaitTimeModal) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "auto";
    }
  }, [showEditPicModal, showEditLocationsModal, showEditAdditionalModal, showEditFeesModal, showCreateNewClinicModal, showEditIntroModal, showEditWaitTimeModal]);

  return (
    <>
      <ProfessioanlInfoContainer>
        <ProfessionalSummary
          editPicModalHandler={setShowEditPicModal}
          typeHandler={setEditPicModalType}
          editIntroModalHandler={setShowEditIntroModal}
        />
        <InformationContainer title="Areas of Interest" editAdditionalModalHandler={setShowEditAdditionalModal} />
        <InformationContainer title="Additional Information" editAdditionalModalHandler={setShowEditAdditionalModal} />
        <WaitTimeContainer editWaitTimeModalHandler={setShowEditWaitTimeModal} />
        <FeesContainer editFeesModalHandler={setShowEditFeesModal} />
        <LocationContainer editLocationsModalHandler={setShowEditLocationsModal} />
      </ProfessioanlInfoContainer>
      {(showEditPicModal || showEditLocationsModal || showEditAdditionalModal || showEditFeesModal || showCreateNewClinicModal || showEditIntroModal || showEditWaitTimeModal) && <Mask />}
      {showEditPicModal && (
        <EditPictureModal
          type={editPicModalType}
          showHandler={setShowEditPicModal}
        />
      )}
      {showEditAdditionalModal && <EditAdditionalInfoModal showHandler={setShowEditAdditionalModal} />}
      {showEditFeesModal && <EditFeesModal showHandler={setShowEditFeesModal} />}
      {showCreateNewClinicModal && <CreateNewClinicModal showHandler={setShowCreateNewClinicModal} />}
      {showEditIntroModal && <EditIntroModal
        showHandler={setShowEditIntroModal}
        editPicModalHandler={setShowEditPicModal}
        typeHandler={setEditPicModalType}
      />}
      {showEditWaitTimeModal && <EditWaitTimeModal showHandler={setShowEditWaitTimeModal} />}
      {showEditLocationsModal && <EditLocationsModal showHandler={setShowEditLocationsModal} />}
    </>
  )
}

const ProfessioanlInfoContainer = styled(FlexColumn)`
  align-items: center;
  height: 100%;
  margin-top: 9px;
  background: #E5E5E5;
`;

const Mask = styled.div`
  position: absolute;
  top: 0;
  height: 100%;
  width: 100%;
  z-index: 10;
  background: black;
  opacity: 40%;
`;