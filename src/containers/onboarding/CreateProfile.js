import React, {useState, useEffect} from "react";
import styled, {css} from "styled-components/macro";
import {FlexColumn, FlexRow} from "../../components/Widgets/BaseStyledComponents";
import {PromptText, SectionSubTitle, SectionTitle} from "../../components/Widgets/Text";
import {useHistory} from "react-router-dom";
import {SingleLineFormField} from "../../components/Widgets/FormFields";
import {useForm} from "react-hook-form";
import {Button} from "../../components/Widgets/Buttons";



export const CreateProfile = ({...props}) => {
  const history = useHistory();
  const {register, handleSubmit, errors} = useForm()

  let firstName = (props.location.query && props.location.query.firstName) || ""
  let lastName = (props.location.query && props.location.query.lastName) || ""

  const onSubmit = (data) => {
    console.log("PROFILE DATA", data)
    history.push({
      pathname: "/create-account",
      query: {"firstName": data.firstName, "lastName": data.lastName, "ahpraNumber": data.ahrpaNumber}
    })
  }

  return (
    <OnboardingSectionContainer>
      <OnboardingSectionWrapper>
        <TitleContainer>
          <SectionTitle underlined={false}>Create your Nexos Health Profile</SectionTitle>
          <SectionSubTitle>Please fill all fields to continue</SectionSubTitle>
        </TitleContainer>
        <OnboardingForm onSubmit={handleSubmit(onSubmit)}>
          <InputsContainer>
            <SingleLineFormField type="text" name="firstName" defaultValue={firstName} title="First Name" register={register}/>
            <SingleLineFormField name="lastName" defaultValue={lastName} title="Last Name" register={register}/>
            <SingleLineFormField name="ahpraNumber" title="AHPRA Registration Number" register={register}/>
            <a target="_blank"  rel="noopener noreferrer" href="https://www.ahpra.gov.au/Registration/Registers-of-Practitioners.aspx">
              <PromptText>Click here to find your AHPRA number</PromptText>
            </a>
          </InputsContainer>
          <ButtonContainer>
            <Button type="submit" size="large">Next</Button>
          </ButtonContainer>
        </OnboardingForm>
      </OnboardingSectionWrapper>
    </OnboardingSectionContainer>
  )
}


export const OnboardingSectionContainer = styled(FlexColumn)`
  align-items: center;
  height: 100%;
`;

export const OnboardingSectionWrapper = styled(FlexColumn)`
  width: 100%;
  max-width: 600px;
  align-items: center;
  padding: 100px 25px;
  ${props => props.color && css`
    background-color: ${props.color};  
  `};
  @media (max-width: 800px) {
    padding: 50px 25px;
  }
`;

export const TitleContainer = styled.div`
  width: 100%;
  border-bottom: 0.5px solid lightgrey;
`;

export const OnboardingForm = styled.form`
  width: 100%;
  text-align: left;
  box-sizing: content-box;
  padding: 20px 0;
`;

export const InputsContainer = styled(FlexColumn)`
  padding-bottom: 20px;
  border-bottom: 0.5px solid lightgrey;
  margin-bottom: 20px;
`;

export const ButtonContainer = styled(FlexRow)`
  justify-content: center;
`;