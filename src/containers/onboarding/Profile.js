import React, { useEffect, useState } from 'react';
import styled, { css } from 'styled-components';
import { CompanyInfo } from '../../components/company/CompanyInfo';
import { AboutContainer } from '../../components/company/About';
import { PeoplesContainer } from '../../components/company/People';
import { ServicesContainer } from '../../components/company/Services';
import { ContactContainer } from '../../components/company/Contact';
import {
  EditPictureModal,
} from '../../components/Widgets/modals';
import {
  EditAboutModal,
  EditIntroModal,
  EditContactInfoModal,
  EditScheduleModal,
  EditServicesModal,
  EditPeoplesModal,
} from '../../components/Widgets/modals/company'
import { FlexColumn } from '../../components/Widgets/BaseStyledComponents';

export const Profile = () => {
  const [tab, setTab] = useState(1);
  const [showEditPicModal, setShowEditPicModal] = useState(false);
  const [editPicModalType, setEditPicModalType] = useState('profile');
  const [showEditIntroModal, setShowEditIntroModal] = useState(false);
  const [showEditAboutModal, setShowEditAboutModal] = useState(false);
  const [showEditContactInfoModal, setShowEditContactInfoModal] = useState(false);
  const [showEditScheduleModal, setShowEditScheduleModal] = useState(false);
  const [showEditServicesModal, setShowEditServicesModal] = useState(false);
  const [showEditPeoplesModal, setShowEditPeoplesModal] = useState(false);

  useEffect(() => {
    if (showEditPeoplesModal || showEditIntroModal || showEditPicModal || showEditAboutModal || showEditContactInfoModal || showEditScheduleModal || showEditServicesModal) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "auto";
    }
  }, [showEditPeoplesModal, showEditIntroModal, showEditPicModal, showEditAboutModal, showEditContactInfoModal, showEditScheduleModal, showEditServicesModal]);

  return (
    <>
      <ProfileContainer>
        <CompanyInfo
          tabHandler={setTab}
          tab={tab}
          editPicModalHandler={setShowEditPicModal}
          typeHandler={setEditPicModalType}
          editIntroModalHandler={setShowEditIntroModal}
        />
        {tab === 1 &&
          <>
            <AboutContainer editAboutModalHandler={setShowEditAboutModal} />
            <PeoplesContainer sort="People" editPeoplesModalHandler={setShowEditPeoplesModal} />
          </>
        }
        {tab ===2 &&
          <>
            <PeoplesContainer sort="General Practicioner" editPeoplesModalHandler={setShowEditPeoplesModal} />
            <PeoplesContainer sort="Psychologist" editPeoplesModalHandler={setShowEditPeoplesModal} />
          </>
        }
        {(tab === 1 || tab === 3) &&
          <ServicesContainer editServicesModalHandler={setShowEditServicesModal} />
        }
        {(tab === 1 || tab === 4) &&
          <ContactContainer
            editContactInfoModalHandler={setShowEditContactInfoModal}
            editScheduleModalHandler={setShowEditScheduleModal}
          />
        }
        <Footer>
          <FooterText>© Copyright – Nexus Health</FooterText>
        </Footer>
      </ProfileContainer>
      {(showEditPeoplesModal || showEditPicModal || showEditIntroModal || showEditAboutModal || showEditContactInfoModal || showEditScheduleModal || showEditServicesModal) && <Mask />}
      {showEditPicModal && (
        <EditPictureModal
          type={editPicModalType}
          showHandler={setShowEditPicModal}
        />
      )}
      {showEditIntroModal && (
        <EditIntroModal
          showHandler={setShowEditIntroModal}
          editPicModalHandler={setShowEditPicModal}
          typeHandler={setEditPicModalType}
        />
      )}
      {showEditAboutModal && (
        <EditAboutModal showHandler={setShowEditAboutModal} />
      )}
      {showEditContactInfoModal && (
        <EditContactInfoModal showHandler={setShowEditContactInfoModal} />
      )}
      {showEditScheduleModal && (
        <EditScheduleModal showHandler={setShowEditScheduleModal} />
      )}
      {showEditServicesModal && (
        <EditServicesModal showHandler={setShowEditServicesModal} />
      )}
      {showEditPeoplesModal && (
        <EditPeoplesModal showHandler={setShowEditPeoplesModal} />
      )}
    </>
  )
}

const ProfileContainer = styled(FlexColumn)`
  align-items: center;
  height: 100%;
  margin-top: 9px;
  background: #E5E5E5;
`;

const Mask = styled.div`
  position: absolute;
  top: 0;
  height: 100%;
  width: 100%;
  z-index: 10;
  background: black;
  opacity: 40%;
`;

const Footer = styled.div`
  padding: 7px 20px 0;
  width: 100%;
  max-width: 600px;
  background: #FFFFFF;
`;

const FooterText = styled.h1`
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
  color: #899099;
  float: right;
`;