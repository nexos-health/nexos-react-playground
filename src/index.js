import React from 'react';
import ReactDOM from 'react-dom';

// FontAwesome Imports
import initFontAwesome from "./utils/initFontAwesome";
import {library} from "@fortawesome/fontawesome-svg-core";
import {far} from "@fortawesome/free-regular-svg-icons";
import {fas} from "@fortawesome/free-solid-svg-icons";

import * as serviceWorker from './serviceWorker';
import history from "./utils/history";
import {Router} from "react-router-dom";
import App from "./App";
import './index.css';



const rootElement = document.getElementById('root');

// Initialising Font Awesome
initFontAwesome();
library.add(far, fas);

// Application to render
const render = Component => {
  ReactDOM.render(
    <Router history={history}>
      <Component/>
    </Router>,
    rootElement
  );
}

render(App);

if (module.hot) {
  module.hot.accept('./App', () => {
    const NextApp = require('./App').default;
    render(NextApp);
  });
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
