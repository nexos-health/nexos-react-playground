import React from 'react';
import styled from 'styled-components';

import { View } from '../Widgets/View';
import { DescriptionText } from '../Widgets/Text';
import { RightArrowIcon } from "../Widgets/BaseStyledComponents";

import sampleImg from '../../assets/sample.png';

export const AboutContainer = ({ ...props }) => {
  const { editAboutModalHandler } = props;

  return (
    <View title="About" editHandler={editAboutModalHandler}>
      <DescriptionText>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo, morbi egestas fermentum at justo arcu scelerisque. Vehicula in et enim placerat amet nisi, pretium sodales. Egestas sed sed lectus massa tellus pretium. Vitae sit pretium non nunc.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo, morbi egestas fermentum at justo arcu scelerisque. Vehicula in et enim placerat amet nisi, pretium sodales. Egestas sed sed lectus massa tellus pretium. Vitae sit pretium non nunc.
      </DescriptionText>
      <Slide>
        <ImageView src={sampleImg}></ImageView>
        <ButtonWrapper>
          <RightChevron icon={["fas", "angle-right"]}/>
        </ButtonWrapper>
      </Slide>
    </View>
  )
}

const Slide = styled.div`
  position: relative;
`;

const ButtonWrapper = styled.div`
  position: absolute;
  top: 50%;
  right: -25px;
`;

const ImageView = styled.img`
  margin-top: 20px;
  width: 100%;
`;

const RightChevron = styled(RightArrowIcon)`
  align-self: center;
  color: #B1DBFF;
  font-size: 24px;
  margin-right: 10px;
`;