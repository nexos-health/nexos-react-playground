import React, { useState } from 'react';
import styled, {css} from 'styled-components/macro';

import { FlexColumn, Divider } from '../Widgets/BaseStyledComponents';
import { Button } from '../Widgets/Buttons';
import { NameTitle, LocationTitle, PromptText, DescriptionText } from '../Widgets/Text';

import cameraIcon from '../../assets/camera.svg';
import logoIcon from '../../assets/group.png';
import locationIcon from '../../assets/location.svg';
import checkIcon from '../../assets/check.svg';
import closeIcon from '../../assets/close.svg';

export const CompanyInfo = ({ ...props }) => {
  const {
    tabHandler,
    tab,
    editPicModalHandler,
    typeHandler,
    editIntroModalHandler
  } = props;

  const editPicHandler = type => {
    editPicModalHandler(true);
    typeHandler(type);
  };

  return (
    <CompanyInfoContainer>
      <PhotoContainer {...props}>
        <EditBackground onClick={() => editPicHandler('cover')}>
          <CameraIcon size="small" src={cameraIcon} />
        </EditBackground>
        <LogoContainer>
          <LogoImage src={logoIcon} />
        </LogoContainer>
        <EditLogo onClick={() => editPicHandler('profile')}>
          <CameraIcon src={cameraIcon} />
        </EditLogo>
      </PhotoContainer>
      <MainContainer>
        <Button variant='link' float='right' underline='underline' weight='sm' onClick={() => editIntroModalHandler(true)}>Edit</Button><br/>
        <NameTitle size='lg'>Rosny Doctors & After Hours</NameTitle>
        <PromptText color='gray'>General Practice</PromptText>
        <Divider/>
        <DescriptionText>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo, morbi egestas fermentum at justo arcu dark.</DescriptionText>
        <AddressContainer>
          <AddressIcon src={locationIcon} />
          <LocationTitle weight='sm'>Rosny Park, Hobart, Tasmania</LocationTitle>
        </AddressContainer>
        <StatusContainer>
          <Status>
            <DescriptionText>Telehealth</DescriptionText>
            <StatusIcon src={checkIcon} />
          </Status>
          <Status>
            <DescriptionText>Bullk billing</DescriptionText>
            <StatusIcon src={closeIcon} />
          </Status>
        </StatusContainer>
        <TabContainer>
          <Tab active={tab === 1} onClick={() => tabHandler(1)}>Overview</Tab>
          <Tab active={tab === 2} onClick={() => tabHandler(2)}>People</Tab>
          <Tab active={tab === 3} onClick={() => tabHandler(3)}>Services</Tab>
          <Tab active={tab === 4} onClick={() => tabHandler(4)}>Contact</Tab>
        </TabContainer>
      </MainContainer>
    </CompanyInfoContainer>
  )
}

const CompanyInfoContainer = styled(FlexColumn)`
  width: 100%;
  max-width: 600px;
  border-radius: 5px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  margin-bottom: 10px;
  background: #FFFFFF;
`;

const PhotoContainer = styled.div`
  position: relative;
  height: 98px;
  border-radius: 5px 5px 0px 0px;
  ${props => props.backgroundImg ? css`
    background: url(${props.backgroundImg});
    background-repeat: no-repeat;
    background-size: cover;
  ` : css`
    background: #B1DBFF;
  `};
`;

const MainContainer = styled.div`
  margin-top: 20px;
  padding: 0 20px;
`;

const LogoContainer = styled.div`
  position: absolute;
  top: 32px;
  left: 20px;
  width: 100px;
  height: 100px;
  text-align: center;
  background: #69B9FF;
  border-radius: 100px;
  border: 2px solid #FFFFFF;
`;

const AddressContainer = styled.div`
  display: flex;
  margin: 10px 0 18px;
`;

const StatusContainer = styled.div`
  display: flex;
`;

const Status = styled.div`
  display: flex;
  margin-right: 15px;
`;

const TabContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  margin: 15px 0 9px;
  text-align: center;
`;

const EditBackground = styled.div`
  position: absolute;
  top: 4px;
  right: 8px;
  border: 0.5px solid #CCCCCC;
  box-sizing: border-box;
  width: 28px;
  height: 28px;
  background: #FFFFFF;
  border-radius: 40px;
  text-align: center;
  cursor: pointer;
`;

const CameraIcon = styled.img`
  ${props => props.size === "small" ? css`
    width: 12px;
    height: 10px;
    margin-bottom: 2px;
  ` : css`
    width: 14px;
    height: 12px;
  `};
`;

const AddressIcon = styled.img`
  margin-right: 5px;
`;

const LogoImage = styled.img`
  margin-top: 15px;
`;

const StatusIcon = styled.img`
  margin-left: 3px;
  margin-bottom: -2px;
`;

const EditLogo = styled.div`
  position: absolute;
  top: 102px;
  left: 90px;
  border: 0.5px solid #CCCCCC;
  box-sizing: border-box;
  width: 30px;
  height: 30px;
  background: #FFFFFF;
  border-radius: 40px;
  text-align: center;
  cursor: pointer;
`;

const Tab = styled.div`
  font-weight: 400;
  line-height: 24px;
  color: #9C9C9C;
  cursor: pointer;
  ${props => props.active && css`
    color: #283343;
    font-weight: 600;
    text-decoration: underline;
    text-underline-offset: 10px;
    text-decoration-thickness: 2px;
  `};
`;