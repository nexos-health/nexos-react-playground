import React from 'react';
import styled from 'styled-components';

import { Divider } from '../Widgets/BaseStyledComponents';
import { View } from '../Widgets/View';
import { NameTitle, FormFieldTitle, PromptText } from '../Widgets/Text';
import { Button } from '../Widgets/Buttons';

import locationIcon from '../../assets/address.svg';
import phoneIcon from '../../assets/phone.svg';
import cellIcon from '../../assets/cellphone.svg';
import siteIcon from '../../assets/site.svg';

export const ContactContainer = ({ ...props }) => {
  const { editContactInfoModalHandler, editScheduleModalHandler } = props;

  return (
    <View title='Contact' editHandler={editContactInfoModalHandler}>
      <InfoItem>
        <ItemIcon src={locationIcon} />
        <ItemText>27 Bligh Street, Rosny Park, Tas 7018</ItemText>
      </InfoItem>
      <InfoItem>
        <ItemIcon src={phoneIcon} />
        <ItemText>(03)62441058</ItemText>
      </InfoItem>
      <InfoItem>
        <ItemIcon src={cellIcon} />
        <ItemText>62450428</ItemText>
      </InfoItem>
      <InfoItem>
        <ItemIcon src={siteIcon} />
        <ItemText>www.website.com.au</ItemText>
      </InfoItem>
      <HeadWrapper><br/>
        <NameTitle size='lg'>Opening Hours</NameTitle>
        <ButtonWrapper>
          <Button variant="link" weight='sm' onClick={editScheduleModalHandler}>Edit</Button><br/>
        </ButtonWrapper>
      </HeadWrapper>
      <Divider secondary />
      <TimeTable>
        <InfoPerDay>
          <FormFieldTitle light>Monday</FormFieldTitle>
          <TimeText>8:30 am - 5:00 pm</TimeText>
        </InfoPerDay>
        <InfoPerDay>
          <FormFieldTitle light>Tuesday</FormFieldTitle>
          <TimeText>8:30 am - 5:00 pm</TimeText>
        </InfoPerDay>
        <InfoPerDay>
          <FormFieldTitle light>Wednesday</FormFieldTitle>
          <TimeText>8:30 am - 5:00 pm</TimeText>
        </InfoPerDay>
        <InfoPerDay>
          <FormFieldTitle light>Thursday</FormFieldTitle>
          <TimeText>8:30 am - 5:00 pm</TimeText>
        </InfoPerDay>
        <InfoPerDay>
          <FormFieldTitle light>Friday</FormFieldTitle>
          <TimeText>8:30 am - 5:00 pm</TimeText>
        </InfoPerDay>
        <InfoPerDay>
          <FormFieldTitle light>Saturday</FormFieldTitle>
          <TimeText inactive>Closed</TimeText>
        </InfoPerDay>
        <InfoPerDay>
          <FormFieldTitle light>Sunday</FormFieldTitle>
          <TimeText inactive>Closed</TimeText>
        </InfoPerDay>
        <InfoPerDay>
          <FormFieldTitle light>Public Holidays</FormFieldTitle>
          <TimeText inactive>Closed</TimeText>
        </InfoPerDay>
      </TimeTable>
      <Divider/>
    </View>
  )
}

const InfoItem = styled.div`
  padding: 7px 0;
  border-bottom: 1px solid #F2F7FF;
`;

const InfoPerDay = styled.div`
  margin: 7px 0;
`;

const HeadWrapper = styled.div`
  position: relative;
`;

const ButtonWrapper = styled.div`
  position: absolute;
  top: 25px;
  right: 0px;
`;

const ItemIcon = styled.img`
  width: 28px;
  height: 28px;
`;

const ItemText = styled(PromptText)`
  float: right;
  margin-top: 3px;
  font-weight: normal;
  font-size: 16px;
  line-height: 21px;
  color: #525252;
`;

const TimeText = styled(NameTitle)`
  float: right;
  margin-top: -20px;
  font-weight: 600;
  color: ${props => props.inactive && '#FF6969'};
`;

const TimeTable = styled.div`
  padding-bottom: 5px;
`;