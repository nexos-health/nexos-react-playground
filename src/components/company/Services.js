import React from 'react';
import styled from 'styled-components';

import { View } from '../Widgets/View';
import { NameTitle, DescriptionText } from '../Widgets/Text';

export const ServicesContainer = ({ ...props }) => {
  const { editServicesModalHandler } = props;

  return (
    <View title='Services' editHandler={editServicesModalHandler}>
      <DescriptionText>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo, morbi egestas fermentum at justo arcu scelerisque. Vehicula in et enim placerat amet nisi, pretium sodales.
      </DescriptionText><br/>
      <NameTitle size='md' weight='md'>Extra service</NameTitle>
      <ExtraServices>
        <ServiceOption backgroundColor='#51D376'>Telehealth</ServiceOption>
        <ServiceOption backgroundColor='#56CCF2'>Bulk billing</ServiceOption>
        <ServiceOption backgroundColor='#FF8989'>Urgent visit</ServiceOption>
      </ExtraServices>
    </View>
  )
}

const ExtraServices = styled.div`
  display: flex;
  margin-top: 16px;
`;

const ServiceOption = styled.div`
  font-weight: 600;
  font-size: 14px;
  line-height: 19px;
  padding: 3px 6px;
  margin-right: 10px;
  width: fit-content;
  border-radius: 25px;
  background: ${props => props.backgroundColor};
  color: #FFFFFF;
`;