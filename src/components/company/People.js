import React from 'react';
import styled from 'styled-components';

import { View } from '../Widgets/View';
import { PromptText } from '../Widgets/Text';
import { PeopleCard } from '../Widgets/company/PeopleCard';

export const PeoplesContainer = ({ ...props }) => {
  const { sort, editPeoplesModalHandler } = props;

  const mockData = [
    {
      name: 'Oliver Johnes',
      job: 'General Practicioner',
      summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo, morbi egestas fermentum.'
    },
    {
      name: 'Linda Smith',
      job: 'Psychologist',
      summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo, morbi egestas fermentum.'
    }
  ];

  return (
    <View title={sort} editHandler={editPeoplesModalHandler}>
      <Description>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo, morbi egestas fermentum at justo arcu scelerisque. Vehicula in et enim placerat amet nisi, pretium sodales.
      </Description>
      {mockData.map(people => <PeopleCard data={people} />)}
    </View>
  )
}

const Description = styled(PromptText)`
  line-height: 16px;
  display: flex;
  align-items: center;
  color: #283343;
  margin-bottom: 16px;
`;