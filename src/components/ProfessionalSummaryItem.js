import React from "react";
import verifiedAccount from "../../src/assets/verified-account.png";
import styled from "styled-components/macro";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {FlexColumn, FlexRow, OverflowText, RightArrowIcon} from "./Widgets/BaseStyledComponents";
import {LIGHT_TEXT_COLOR} from "../utils/constants";
import {NameTitle} from "./Widgets/Text";


export const ProfessionalSummaryItem = ({ professional, handleSelectProfessional }) => {
  let professionTypes = professional.professions.map((profession) => {
    return profession.professionType
  })
  let professionTypesText = professionTypes.join(", ")

  if (professional.user) {
    return null
  }

  return (
    <ItemContainer>
      <ProfileIcon icon={["fas", "user-circle"]}/>
      <ProfessionalSummaryInfo>
        <NameContainer onClick={() => handleSelectProfessional(professional.uid)}>
          <Name>{professional.firstName}&nbsp;</Name>
          <Name>{professional.lastName}</Name>
          {professional.user && <VerifiedIcon src={verifiedAccount} alt="verified-account"/>}
        </NameContainer>
        <ProfessionType>{professionTypesText}</ProfessionType>
        <Location>{"Hobart"}</Location>
      </ProfessionalSummaryInfo>
      <RightChevron icon={["fas", "angle-right"]}/>
    </ItemContainer>
  )
}

const ItemContainer = styled(FlexRow)`
  padding: 10px 0;
  border-bottom: 1px solid lightgrey;
`;

const ProfessionalSummaryInfo = styled(FlexColumn)`
  width: 100%;
`;

const ProfileIcon = styled(FontAwesomeIcon)`
  align-self: start;
  font-size: 40px;
  margin-right: 0.3em;
  color: cornflowerblue;
`;

const NameContainer = styled(FlexRow)`
  flex-wrap: wrap;
  &:hover {
    color: royalblue;
    cursor: pointer;
  }
`;

const Name = styled(NameTitle)`
  line-height: 20px;
`;

const VerifiedIcon = styled.img`
  align-self: flex-start;
  margin: 0 0 0 3px;
  width: 20px;
`;

const ProfessionType = styled(OverflowText)`
  color: ${LIGHT_TEXT_COLOR};
  font-size: 14px;
  max-width: 200px;
  align-self: flex-start;
`;

const Location = styled.div`
  font-size: 13px;
`;

const RightChevron = styled(RightArrowIcon)`
  align-self: center;
  color: dimgrey;
  font-size: 18px;
  margin-right: 10px;
`;