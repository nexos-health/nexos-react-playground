import React from 'react';
import styled, {css} from 'styled-components/macro';

import { FlexColumn, Divider } from '../Widgets/BaseStyledComponents';
import { Button } from '../Widgets/Buttons';
import { NameTitle, LocationTitle, PromptText, DescriptionText } from '../Widgets/Text';

import cameraIcon from '../../assets/camera.svg';
import avatarImg from '../../assets/avatar2.png';
import locationIcon from '../../assets/location.svg';
import greenDot from '../../assets/greendot.svg';
import purpleDot from '../../assets/purpledot.svg';

export const ProfessionalSummary = ({ ...props }) => {
  const {
    editPicModalHandler,
    typeHandler,
    editIntroModalHandler,
  } = props;

  const editPicHandler = type => {
    editPicModalHandler(true);
    typeHandler(type);
  };

  return (
    <ProfessionalSummaryContainer>
      <PhotoContainer {...props}>
        <EditBackground onClick={() => editPicHandler('cover')}>
          <CameraIcon size="small" src={cameraIcon} />
        </EditBackground>
        <LogoImage src={avatarImg} />
        <EditLogo onClick={() => editPicHandler('profile')}>
          <CameraIcon src={cameraIcon} />
        </EditLogo>
      </PhotoContainer>
      <MainContainer>
        <Button variant='link' float='right' underline='underline' weight='sm' onClick={editIntroModalHandler}>Edit</Button><br/>
        <NameTitle size='lg'>Dr George Benson</NameTitle>
        <DescriptionText>Rosny Doctors & After Hours</DescriptionText>
        <Divider />
        <NameTitle>Dr George Benson</NameTitle>
        <AddressContainer>
          <AddressIcon src={locationIcon} />
          <LocationTitle weight="sm">Rosny Park, Hobart, Tasmania</LocationTitle>
        </AddressContainer>
        <StatusContainer>
          <Status>
            <DescriptionText>Telehealth</DescriptionText>
            <StatusIcon src={greenDot} />
          </Status>
          <Status>
            <DescriptionText>Urgent Appointments</DescriptionText>
            <StatusIcon src={purpleDot} />
          </Status>
        </StatusContainer>
        <FooterLink>
          <PromptText color='white'>more locations &#x2192;</PromptText>
        </FooterLink>
      </MainContainer>
    </ProfessionalSummaryContainer>
  )
}

const ProfessionalSummaryContainer = styled(FlexColumn)`
  width: 100%;
  max-width: 600px;
  padding-bottom: 35px;
  margin-bottom: 10px;
  background: #FFFFFF;
`;

const PhotoContainer = styled.div`
  position: relative;
  height: 98px;
  ${props => props.backgroundImg ? css`
    background: url(${props.backgroundImg});
    background-repeat: no-repeat;
    background-size: cover;
  ` : css`
    background: #B1DBFF;
  `};
`;

const MainContainer = styled.div`
  margin-top: 20px;
  padding: 0 20px;
`;

const AddressContainer = styled.div`
  display: flex;
  margin: 10px 0 18px;
`;

const StatusContainer = styled.div`
  display: flex;
`;

const EditBackground = styled.div`
  position: absolute;
  top: 4px;
  right: 8px;
  border: 0.5px solid #CCCCCC;
  box-sizing: border-box;
  width: 28px;
  height: 28px;
  background: #FFFFFF;
  border-radius: 40px;
  text-align: center;
  cursor: pointer;
`;

const Status = styled.div`
  display: flex;
  margin-right: 15px;
`;

const FooterLink = styled.div`
  padding: 3px 10px 5px;
  margin-top: 10px;
  width: fit-content;
  background: #2F80ED;
  border-radius: 15px;
`;

const CameraIcon = styled.img`
  ${props => props.size === "small" ? css`
    width: 12px;
    height: 10px;
    margin-bottom: 2px;
  ` : css`
    width: 14px;
    height: 12px;
  `};
`;

const AddressIcon = styled.img`
  margin-right: 5px;
`;

const StatusIcon = styled.img`
  margin-left: 6px;
  margin-bottom: -2px;
`;

const LogoImage = styled.img`
  position: absolute;
  top: 32px;
  left: 20px;
  width: 100px;
  height: 100px;
  border-radius: 100px;
  border: 2px solid #FFFFFF;
  filter: drop-shadow(0px 2px 3px rgba(0, 0, 0, 0.25));
`;

const EditLogo = styled.div`
  position: absolute;
  top: 102px;
  left: 90px;
  border: 0.5px solid #CCCCCC;
  box-sizing: border-box;
  width: 30px;
  height: 30px;
  background: #FFFFFF;
  border-radius: 40px;
  text-align: center;
  cursor: pointer;
`;