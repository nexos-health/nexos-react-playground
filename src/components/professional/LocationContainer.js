import React from 'react';
import styled from 'styled-components';

import { LocationCard } from '../Widgets/professional/LocationCard';
import { View } from '../Widgets/View';

export const LocationContainer = ({ ...props }) => {
  const { editLocationsModalHandler } = props;

  return (
    <View title='Location' editHandler={editLocationsModalHandler}>
      <MainContainer>
        <LocationCard />
        <LocationCard />
      </MainContainer>
      <FooterText>© Copyright – Nexus Health</FooterText>
    </View>
  )
}

const MainContainer = styled.div`
  margin-bottom: 25px;
`;

const FooterText = styled.h1`
  position: absolute;
  right: 20px;
  bottom: 3px;
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
  color: #899099;
  float: right;
`;