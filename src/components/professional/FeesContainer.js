import React from 'react';

import { FeeCard } from '../Widgets/professional/FeeCard';
import { View } from '../Widgets/View';
import { DescriptionText } from '../Widgets/Text';
import { Divider } from '../Widgets/BaseStyledComponents';

export const FeesContainer = ({ ...props }) => {
  const { editFeesModalHandler } = props;
  return (
    <View title="Fees" editHandler={editFeesModalHandler}>
      <DescriptionText>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo, morbi egestas fermentum at justo arcu scelerisque.
      </DescriptionText>
      <Divider />
      <div>
        <FeeCard />
        <FeeCard />
      </div>
    </View>
  )
}