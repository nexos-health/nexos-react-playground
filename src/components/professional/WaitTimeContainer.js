import React from 'react';
import styled from 'styled-components';

import { View } from '../Widgets/View';
import { NameTitle, PromptText, DescriptionText } from '../Widgets/Text';
import { Divider } from '../Widgets/BaseStyledComponents';

import locationIcon from '../../assets/blacklocation.svg';

export const WaitTimeContainer = ({ ...props }) => {
  const { editWaitTimeModalHandler } = props;

  return (
    <View title="Wait time" editHandler={editWaitTimeModalHandler}>
      <DescriptionText>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo, morbi egestas fermentum at justo arcu scelerisque.
      </DescriptionText>
      <Divider />
      <TimeContainer>
        <TimeOption>
          <AddressIcon src={locationIcon} />
          <NameTitle weight='md'>Rosny Doctors & After Hours</NameTitle>
          <TimeText>~ 2 weeks</TimeText>
        </TimeOption>
        <TimeOption>
          <AddressIcon src={locationIcon} />
          <NameTitle weight='md'>Rosny Doctors & After Hours</NameTitle>
          <TimeText>~ 4 weeks</TimeText>
        </TimeOption>
        <TimeOption>
          <AddressIcon src={locationIcon} />
          <NameTitle weight='md'>Rosny Doctors & After Hours</NameTitle>
          <TimeText>~ 3 weeks</TimeText>
        </TimeOption>
      </TimeContainer>
    </View>
  )
}

const TimeContainer = styled.div`
  padding: 0 0 8px;
  border-bottom: 1px solid #E2E9F3;
`;

const TimeOption = styled.div`
  display: flex;
  margin: 8px 0;
`;

const AddressIcon = styled.img`
  margin-right: 8px;
`;

const TimeText = styled(PromptText)`
  padding-top: 2px;
  margin-left: 5px;
  color: #FFC400;
  font-weight: 600;
  font-size: 16px;
  line-height: 21px;
`;