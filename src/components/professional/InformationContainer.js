import React from 'react';
import styled from 'styled-components';

import { View } from '../Widgets/View';
import { DescriptionText } from '../Widgets/Text';

export const InformationContainer = ({ ...props }) => {
  const { title, editAdditionalModalHandler } = props;

  return (
    <View title={title} editHandler={editAdditionalModalHandler}>
      <DescriptionText>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo, morbi egestas fermentum at justo arcu scelerisque. Vehicula in et enim placerat amet nisi, pretium sodales. Egestas sed sed lectus massa tellus pretium. Vitae sit pretium non nunc.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo, morbi egestas fermentum at justo arcu scelerisque. Vehicula in et enim placerat amet nisi, pretium sodales. Egestas sed sed lectus massa tellus pretium. Vitae sit pretium non nunc.
      </DescriptionText>
    </View>
  )
}