import React from "react";
import styled, { css } from "styled-components"
import {BLUE_TEXT_COLOR, DARK_TEXT_COLOR, LIGHT_TEXT_COLOR, GRAY_TEXT_COLOR, WHITE_TEXT_COLOR} from "../../utils/constants";


// Styled Components

export const PromptText = styled.div`
  color: ${props => props.color === 'gray' ? GRAY_TEXT_COLOR : props.color === 'white' ? WHITE_TEXT_COLOR : BLUE_TEXT_COLOR};
  font-size: 14px;
  font-weight: 400;
`;

export const NameTitle = styled.div`
  color: ${props => props.color === 'gray' ? GRAY_TEXT_COLOR : DARK_TEXT_COLOR};
  font-size: ${props => props.size === 'lg' ? '20px' : props.size === 'md' ? '18px' : '16px'};
  font-weight: ${props => props.weight === 'sm' ? '400' : props.weight === 'md' ? '600' : '700'};
`;

export const LocationTitle = styled.div`
  color: ${BLUE_TEXT_COLOR};
  font-size: 16px;
  line-height: 19px;
  font-weight: ${props => props.weight === 'sm' ? 'normal' : '700'};
`;

export const FormFieldTitle = styled.h6`
  color: ${DARK_TEXT_COLOR};
  font-size: 16px;
  font-weight: ${props => props.light ? 'normal' : '600'};
  margin-bottom: 5px;
`;

export const DescriptionText = styled.div`
  line-height: 19px;
  font-size: 14px;
  font-weight: ${props => props.bold ? '600' : '400'};
  display: flex;
  align-items: center;
  color: ${props => props.color === '' ? GRAY_TEXT_COLOR : DARK_TEXT_COLOR};
`;

export const SectionSubTitle = styled.h6`
  width: 100%;
  text-align: left;
  font-weight: 500;
  color: ${LIGHT_TEXT_COLOR};
`;

// Custom Components
export const SectionTitle = ({children, classname, underlined=true}) => {
  return (
    <StyledSectionTitle classname={classname} underlined={underlined}>
      {children}
    </StyledSectionTitle>
  )
}

const StyledSectionTitle = styled.h5`
  width: 100%;
  text-align: left;
  font-weight: 600;
  ${props => props.underlined && css`
    border-bottom: 0.5px solid lightgrey;
  `}
`;

