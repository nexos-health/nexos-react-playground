import React from 'react';
import styled from 'styled-components';

import { NameTitle } from './Text';
import { Button } from './Buttons';
import { Divider } from './BaseStyledComponents';

export const View = ({ children, title, editHandler }) => {
  return (
    <ViewContainer>
      <HeadWrapper>
        <NameTitle size='lg'>{title}</NameTitle>
        <ButtonWrapper>
          <Button variant="link" weight='sm' onClick={() => editHandler(true)}>Edit</Button><br/>
        </ButtonWrapper>
      </HeadWrapper>
      <Divider secondary />
      {children}
    </ViewContainer>
  )
}

const ViewContainer = styled.div`
  position: relative;
  width: 100%;
  max-width: 600px;
  padding: 0 20px 18px;
  background: #FFFFFF;
  margin-bottom: 10px;
`;

const HeadWrapper = styled.div`
  position: relative;
  padding-top: 15px;
`;

const ButtonWrapper = styled.div`
  position: absolute;
  top: 16px;
  right: 0px;
`;