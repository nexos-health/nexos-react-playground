import React from "react";
import styled, { css } from "styled-components"
import {DARK_TEXT_COLOR, LIGHT_TEXT_COLOR} from "../../utils/constants";


export const SingleLineInput = ({className, placeholder="", type="text", name="", register=function(){}})=> {
  return (
    <StyledSingleLineInput
      className={className}
      type={type}
      placeholder={placeholder}
      {...register(name)}
    />
  )
}

const StyledSingleLineInput = styled.input`
  width: 100%;
  border-radius: 5px;
  border: 1px solid hsl(0,0%,40%);
  font-size: 16px;
  line-height: 21px;
  color: ${DARK_TEXT_COLOR};
  padding: 5px 7px;
  transition: all 0.2s ease-in;
`;


export const LargeInput = ({className, placeholder="", type="text", name="", register=function(){}})=> {
  return (
    <StyledLargeInput
      className={className}
      type={type}
      placeholder={placeholder}
      {...register(name)}
    />
  )
}

const StyledLargeInput = styled.input`
  width: 100%;
  min-width: 150px;
  height: 50px;
  border-radius: 5px;
  border: 1px solid hsl(0,0%,95%);
  box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.25);
  color: ${LIGHT_TEXT_COLOR};
  padding: 12px 15px;
  transition: all 0.2s ease-in;
`;

export const TextArea = ({className, size, placeholder="", type="text", name="", register=function(){}}) => {
  return (
    <StyledTextArea
      className={className}
      type={type}
      size={size}
      placeholder={placeholder}
      {...register(name)}
    />
  )
}

const StyledTextArea = styled.textarea`
  width: 100%;
  ${props => props.size === "lg" ? css`
    height: 236px;
  ` : css`
    height: 118px;
  `}
  border-radius: 5px;
  border: 1px solid hsl(0,0%,40%);
  font-size: 16px;
  line-height: 21px;
  color: ${DARK_TEXT_COLOR};
  padding: 5px 7px;
  transition: all 0.2s ease-in;
`;

export const TimeInput = ({className, type="time", name="", defaultValue="", register=function(){}}) => {
  return (
    <StyledTimeInput
      defaultValue={defaultValue}
      className={className}
      type={type}
      {...register(name)}
      step="600"
    />
  )
}

const StyledTimeInput = styled.input`
  width: 75px;
  font-size: 14px;
  line-height: 16px;
  padding: 5px;
  letter-spacing: 0.3rem;
  text-align: center;
  border: 1px solid #697382;
  box-sizing: border-box;
  border-radius: 6px;
  &::-webkit-calendar-picker-indicator {
    background: none;
    display: none;
  }
`;

export const OutlineInput = ({className, type="text", name="", defaultValue="", placeholder="", register=function(){}}) => {
  return (
    <StyledOutlineInput
      placeholder={placeholder}
      defaultValue={defaultValue}
      className={className}
      type={type}
      {...register(name)}
    />
  )
}

const StyledOutlineInput = styled.input`
  width: 100%;
  text-align: right;
  border: none;
  border-bottom: 1px solid #E2E9F3;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  padding-bottom: 3px;
  color: #9AA7B9;
  &:focus, &:hover {
    outline: none;
    border-bottom: 1px solid #E2E9F3;
  }
`;

export const CheckBox = ({className, type="checkbox", name="", defaultValue="", register=function(){}}) => {
  return (
    <StyledCheckBox
      defaultValue={defaultValue}
      className={className}
      type={type}
      {...register(name)}
    />
  )
}

const StyledCheckBox = styled.input`
  width: 19px;
  height: 19px;
`;