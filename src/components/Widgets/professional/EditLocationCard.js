import React from 'react';
import styled from 'styled-components';

import { NameTitle, DescriptionText } from '../Text';

import deleteIcon from '../../../assets/delete.svg';

export const EditLocationCard = ({ ...props }) => {
  return (
    <LocationContainer>
      <NameTitle>Rosny Doctors & After Hours</NameTitle>
      <DescriptionText color='gray'>27 Bligh Street, Rosny Park, Tas 7018</DescriptionText>
      <ButtonWrapper>
        <DeleteIcon src={deleteIcon} />
      </ButtonWrapper>
    </LocationContainer>
  )
}

const LocationContainer = styled.div`
  position: relative;
  padding: 10px 0;
  border-bottom: 1px solid #E2E9F3;
`;

const ButtonWrapper = styled.div`
  position: absolute;
  top: 15px;
  right: 0;
`;

const DeleteIcon = styled.img`
`;