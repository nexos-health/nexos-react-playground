import React from 'react';
import styled from 'styled-components';

import { NameTitle, FormFieldTitle } from '../Text';
import { Button } from '../Buttons';

import locationIcon from '../../../assets/blacklocation.svg';

export const FeeCard = ({ ...props }) => {
  return (
    <FeeContainer>
      <Addressbar>
        <AddressIcon src={locationIcon} />
        <NameTitle size='md' weight='md'>Rosny Doctors & After Hours</NameTitle>
      </Addressbar>
      <InfoContainer>
        <OptionsBar>
          <Option backgroundColor='#FF8989'>no bulk billing</Option>
          <Option backgroundColor='#5CE1D2'>discount</Option>
        </OptionsBar>
        <NameTitle weight='md'>Standard</NameTitle>
        <ContentBar>
          <IndexTitle light>Duration</IndexTitle>
          <NameTitle weight='md'>20 minutes</NameTitle>
        </ContentBar>
        <ContentBar>
          <IndexTitle light>Amount</IndexTitle>
          <NameTitle weight='md'>$ 85</NameTitle>
        </ContentBar>
      </InfoContainer>
      <ButtonWrapper>
        <Button variant='outline-primary' radius='25' weight='sm' fontSize='sm'>more &#x2192;</Button>
      </ButtonWrapper>
    </FeeContainer>
  )
}

const FeeContainer = styled.div`
  position: relative;
  padding: 8px 0 19px;
  border-bottom: 1px solid #E2E9F3;
  margin-bottom: 8px;
`;

const OptionsBar = styled.div`
  display: flex;
  margin: 16px 0 14px;
`;

const InfoContainer = styled.div`
  margin: 8px 0px 8px 22px;
`;

const ContentBar = styled.div`
  display: flex;
`;

const Addressbar = styled.div`
  display: flex;
  margin: 8px 0;
`;

const ButtonWrapper = styled.div`
  position: absolute;
  right: 0;
  bottom: 16px;
`;

const AddressIcon = styled.img`
  margin-right: 8px;
`;

const Option = styled.div`
  padding: 2px 10px;
  margin-right: 10px;
  width: fit-content;
  font-weight: 600;
  font-size: 12px;
  line-height: 16px;
  border-radius: 25px;
  background: ${props => props.backgroundColor};
  color: #FFFFFF;
`;

const IndexTitle = styled(FormFieldTitle)`
  padding-top: 3px;
  margin-right: 10px;
`;