import React from 'react';
import styled from 'styled-components';

import { NameTitle, DescriptionText } from '../Text';

import LogoImg from '../../../assets/companylogo.png';
import phoneIcon from '../../../assets/blackphone.svg';
import cellphoneIcon from '../../../assets/blackcellphone.svg';
import siteIcon from '../../../assets/blacksite.svg';

export const LocationCard = ({ ...props }) => {
  return (
    <LocationContainer>
      <LogoImage src={LogoImg} />
      <div>
        <NameTitle size='md' weight='md'>Rosny Doctors & After Hours</NameTitle>
        <NameTitle color='gray' weight='sm'>27 Bligh Street, Rosny Park, Tas 7018</NameTitle>
        <InfoContainer>
          <InfoBar>
            <IconImage src={phoneIcon} />
            <DescriptionText>(03)62441058</DescriptionText>
          </InfoBar>
          <InfoBar>
            <IconImage src={cellphoneIcon} />
            <DescriptionText>62450428</DescriptionText>
          </InfoBar>
          <InfoBar>
            <IconImage src={siteIcon} />
            <DescriptionText>www.website.com.au</DescriptionText>
          </InfoBar>
        </InfoContainer>
      </div>
    </LocationContainer>
  )
}

const LocationContainer = styled.div`
  margin-bottom: 16px;
  display: flex;
  border-bottom: 1px solid #E2E9F3;
`;

const InfoContainer = styled.div`
  margin: 13px 0;
`;

const InfoBar = styled.div`
  display: flex;
  padding: 3px 0;
`;

const LogoImage = styled.img`
  margin-right: 10px;
  width: 60px;
  height: 60px;
`;

const IconImage = styled.img`
  margin-right: 6px;
`;