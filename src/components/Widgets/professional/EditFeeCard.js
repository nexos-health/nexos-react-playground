import React from 'react';
import styled from 'styled-components';

import { NameTitle, FormFieldTitle } from '../Text';

import locationIcon from '../../../assets/graylocation.svg';
import editIcon from '../../../assets/edit.svg';

export const EditFeeCard = ({ ...props }) => {
  return (
    <FeeContainer>
      <Addressbar>
        <AddressIcon src={locationIcon} />
        <NameTitle size='md' color='gray'>Rosny Doctors & After Hours</NameTitle>
      </Addressbar>
      <NameTitle weight='md' color='gray'>Standard</NameTitle>
      <InfoContainer>
        <IndexTitle light>Duration 20 minutes</IndexTitle>
        <IndexTitle light>Amount $ 85</IndexTitle>
      </InfoContainer>
      <EditButton src={editIcon} />
    </FeeContainer>
  )
}

const FeeContainer = styled.div`
  position: relative;
  padding: 8px 0 15px;
  border-bottom: 1px solid #E2E9F3;
  margin-bottom: 8px;
`;

const InfoContainer = styled.div`
`;

const Addressbar = styled.div`
  display: flex;
  margin: 8px 0;
`;

const AddressIcon = styled.img`
  margin-right: 8px;
`;

const IndexTitle = styled(FormFieldTitle)`
  padding-top: 3px;
  margin-right: 10px;
  color: #9AA7B9;
`;

const EditButton = styled.img`
  position: absolute;
  top: 15px;
  right: 0;
`;