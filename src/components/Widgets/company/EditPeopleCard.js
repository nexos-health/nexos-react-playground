import React from 'react';
import styled from 'styled-components';

import { NameTitle, DescriptionText } from '../Text';

import deleteIcon from '../../../assets/delete.svg';
import avatarImg from '../../../assets/avatar.png';

export const EditPeopleCard = ({ ...props }) => {
  return (
    <EditPeopleContainer>
      <AvatarImage src={avatarImg} />
      <DescriptionText bold>Oliver Jones</DescriptionText>
      <DescriptionText> - General Practicioner</DescriptionText>
      <ButtonWrapper>
        <DeleteIcon src={deleteIcon} />
      </ButtonWrapper>
    </EditPeopleContainer>
  )
}

const EditPeopleContainer = styled.div`
  position: relative;
  display: flex;
  padding: 10px 0;
  border-bottom: 1px solid #E2E9F3;
`;

const ButtonWrapper = styled.div`
  position: absolute;
  top: 15px;
  right: 0;
`;

const DeleteIcon = styled.img`
`;

const AvatarImage = styled.img`
  margin-right: 17px;
`;