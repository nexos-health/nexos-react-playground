import React from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

import { NameTitle, PromptText, DescriptionText } from '../Text';

import avatarImg from '../../../assets/avatar.png';
import flagIcon from '../../../assets/whiteflag.svg';
import clockIcon from '../../../assets/time1.svg';
import timeIcon from '../../../assets/time2.svg';

export const PeopleCard = ({ data }) => {
  const history = useHistory();
  const { name, job, summary } = data;

  const viewProfessional = () => {
    history.push({
      pathname: "/professional",
      query: { "id": "professtional_id" }
    })
  };

  return (
    <CardContainer>
      <CardHeader>
        <PeopleImage src={avatarImg} />
        <PersonalInfo>
          <NameTitle size='md' weight='md'>{name}</NameTitle>
          <NameTitle color='gray' weight='md'>{job}</NameTitle>
        </PersonalInfo>
        <MarkImage src={flagIcon} />
      </CardHeader>
      <CardBody>
        <DescriptionText>{summary}</DescriptionText>
      </CardBody>
      <CardFooter>
        <StatusOption backgroundColor="#FFCB21">
          <ClockImage src={clockIcon} />
          <PromptText color='white'>2 weeks</PromptText>
        </StatusOption>
        <StatusOption backgroundColor="#B1DBFF">
          <ClockImage src={timeIcon} />
          <PromptText color='white'>4 days ago</PromptText>
        </StatusOption>
        <GotoViewLink>
          <PromptText onClick={viewProfessional}>view &#x2192;</PromptText>
        </GotoViewLink>
      </CardFooter>
    </CardContainer>
  )
}

const CardContainer = styled.div`
  position: relative;
  padding: 20px 20px 16px;background: #FFFFFF;
  border: 1px solid #E2E9F3;
  box-sizing: border-box;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 10px;
  margin-bottom: 16px;
`;

const CardHeader = styled.div`
  display: flex;
`;

const CardFooter = styled.div`
  display: flex;
  margin-top: 14px;
`;

const CardBody = styled.div`
  margin-top: 7px;
`;

const StatusOption = styled.div`
  display: flex;
  padding: 3px 10px;
  margin-right: 10px;
  background: ${props => props.backgroundColor};
  border-radius: 15px;
`;

const PersonalInfo = styled.div`
  margin-left: 20px
`;

const GotoViewLink = styled.div`
  position: absolute;
  bottom: 20px;
  right: 21px;
`;

const PeopleImage = styled.img`
  width: 45px;
  height: 45px;
`;

const MarkImage = styled.img`
  position: absolute;
  top: 23px;
  right: 21px;
`;

const ClockImage = styled.img`
  width: 18px;
  height: 18px;
  margin-top: 1px;
  margin-right: 3px;
`;