import React from "react";
import Select, { components } from 'react-select';
import AsyncSelect from 'react-select/async';
import styled, { css } from "styled-components"

import {DARK_TEXT_COLOR, LIGHT_TEXT_COLOR} from "../../utils/constants";

import downIcon from "../../assets/down.svg"

export const MultiSelect = ({ field, options, value, inputValue, onChange, onInputChange, placeholder, register=function(){} }) => {
  const multiSelectStyles = {
    container: () => ({
      ':focus': {
        borderColor: 'black',
        borderWidth: 2,
        borderStyle: 'solid',
        borderRadius: 6,
      },
    }),
    menu: (provided, state) => ({
      ...provided,
      color: state.selectProps.menuColor,
      padding: 20,
    }),
    control: (provided, state) => ({
      ...provided,
      paddingTop: 5,
      paddingBottom: 10,
      height: 80,
      minHeight: 80,
      background: '#fff',
      borderColor: '#666666',
      boxShadow: state.isFocused ? '0 0 0 1px black' : null,
      ':hover': {
        outline: 'none',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: '#666666',
      },
    }),
    valueContainer: (provided, state) => ({
      ...provided,
      overflow: 'auto',
      height: 70,
      alignItems: 'flex-start',
    }),
    indicatorSeparator: state => ({
      display: 'none',
    }),
    indicatorsContainer: (provided, state) => ({
      display: 'none',
    }),
    multiValue: (styles, { data }) => {
      return {
        ...styles,
        borderRadius: '100px',
        backgroundColor: '#B1DBFF',
      };
    },
    multiValueLabel: (styles, { data }) => ({
      ...styles,
      fontSize: '14px',
      color: '#FFFFFF',
    }),
    multiValueRemove: (styles, { data }) => ({
      ...styles,
      marginTop: '2px',
      fontSize: '14px',
      color: '#FFFFFF',
      ':hover': {
        backgroundColor: '#B1DBFF',
        borderRadius: '100px',
        color: 'white',
      },
    }),
  }
  return (
    <Select
      {...field}
      placeholder=""
      options={options}
      value={value}
      closeMenuOnSelect={false}
      isMulti
      isSearchable
      name="services"
      styles={multiSelectStyles}
    />
  )
}

const DropdownIndicator = ({ ...props }) => {
  return (
    <components.DropdownIndicator {...props}>
      <img src={downIcon} />
    </components.DropdownIndicator>
  );
};

export const SingleSelect = ({ field, options, value, placeholder, onChange }) => {
  const singleSelectStyles = {
    menu: (provided, state) => ({
      ...provided,
      color: state.selectProps.menuColor,
    }),
    control: (provided, state) => ({
      ...provided,
      background: '#fff',
      borderColor: '#666666',
      boxShadow: state.isFocused ? '#283343' : null,
      ':hover': {
        outline: 'none',
        borderWidth: '2px',
        borderColor: '#283343',
      },
      ':focus': {
        outline: 'none',
        borderWidth: '2px',
        borderColor: 'red',
      },
    }),
    indicatorSeparator: state => ({
      display: 'none',
    }),
  };

  return (
    <Select
      {...field}
      placeholder={placeholder}
      components={{ DropdownIndicator }}
      options={options}
      value={value}
      name="services"
      styles={singleSelectStyles}
      isSearchable={false}
    />
  )
}

export const StyledAsyncSelect = ({ field, placeholder="", onChange, loadOptions, onInputChange }) => {
  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      borderTop: '1px solid #697382',
      padding: '7px 25px 13px',
      marginTop: -5,
      color: 'black',
      backgroundColor: '#F3F7FF',
    }),
    menu: (provided, state) => ({
      ...provided,
      marginTop: 0,
      padding: 0,
      border: '1px solid #697382',
      borderTop: 'none',
      borderRadius: '0 0 6px 6px',
      background: '#FFFFFF',
      zIndex: '100',
    }),
    control: (provided, state) => ({
      ...provided,
      padding: '2px 13px',
      marginBottom: 0,
      background: '#fff',
      borderColor: '#666666',
      fontSize: 16,
      boxShadow: state.isFocused ? '#283343' : null,
      borderRadius: state.isFocused ? '6px 6px 0 0' : 6,
      ':hover': {
        outline: 'none',
        borderWidth: '1px',
        borderColor: '#697382',
      },
    }),
    indicatorSeparator: state => ({
      display: 'none',
    }),
    dropdownIndicator: (provided, state) => ({
      display: 'none',
    }),
  };

  return (
    <AsyncSelect
      {...field}
      placeholder={placeholder}
      onChange={onChange}
      loadOptions={loadOptions}
      isSearchable
      isClearable
      onInputChange={onInputChange}
      styles={customStyles}
    />
  )
}