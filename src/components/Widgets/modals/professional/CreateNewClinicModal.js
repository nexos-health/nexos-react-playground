import React, {useState, useEffect} from "react";
import { useForm } from "react-hook-form";
import styled, { css } from 'styled-components/macro';

import { NameTitle } from '../../Text';
import { FlexColumn } from "../../BaseStyledComponents";
import { SingleLineFormField } from '../../FormFields';
import { Button } from '../../Buttons';
import { ColorDivider } from '../../BaseStyledComponents';

import closeIcon from '../../../../assets/close2.svg';

const CreateNewClinicModal = ({ ...props }) => {
  const {register, handleSubmit, errors} = useForm()
  const { showHandler } = props;

  const onSubmit = (data) => {
    console.log("ACCOUNT DATA", data);
  }

  return (
    <CreateClinicContainer>
      <ModalHeader>
        <NameTitle size='lg'>Create New Clinic</NameTitle>
        <CloseIcon src={closeIcon} onClick={() => showHandler(false)} />
      </ModalHeader>
      <ColorDivider />
      <OnboardingForm onSubmit={handleSubmit(onSubmit)}>
        <InputsContainer>
          <SingleLineFormField type="text" name="name" title="Name of Clinic" placeholder="Placeholder text ..." register={register}/>
          <SingleLineFormField type="text" name="address" title="Address" placeholder="Placeholder text ..." register={register}/>
          <SingleLineFormField type="text" name="phone" title="Mobile Phone" placeholder="Placeholder text ..." register={register}/>
          <SingleLineFormField type="text" name="fax" title="Fax Number" placeholder="Placeholder text ..." register={register}/>
          <SingleLineFormField type="text" name="website" title="Website" placeholder="Placeholder text ..." register={register}/>
        </InputsContainer>
        <ActionContainer>
          <SaveButton>
            <Button type="submit" size="large" radius="25" weight="sm">Add</Button>
          </SaveButton>
        </ActionContainer>
      </OnboardingForm>
    </CreateClinicContainer>
  )
}

export default CreateNewClinicModal

const CreateClinicContainer = styled.div`
  position: fixed;
  padding: 20px 0;
  top: 15vh;
  left: 0;
  right: 0;
  margin: auto;
  background: #FFFFFF;
  border-radius: 5px;
  max-width: 375px;
  height: 535px;
  z-index: 101;
  @media (max-width: 400px) {
    top: 13vh;
  }
`;

export const OnboardingForm = styled.form`
  margin-top: 15px;
  text-align: left;
  box-sizing: content-box;
  padding: 20px;
`;

export const InputsContainer = styled(FlexColumn)`
  padding-bottom: 20px;
  margin-bottom: 20px;
`;

const ActionContainer = styled.div`
  position: relative;
  display: flex;
  padding: 10px 20px;
`;

const SaveButton = styled.div`
  position: absolute;
  width: 88px;
  bottom: 30px;
  right: 0;
  height: 30px;
`;

const ModalHeader = styled.div`
  position: relative;
  padding: 0 20px;
  margin-bottom: 16px;
`;

const CloseIcon = styled.img`
  position: absolute;
  right: 5px;
  bottom: -5px;
  cursor: pointer;
`;