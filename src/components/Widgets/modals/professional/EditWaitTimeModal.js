import React from "react";
import { useForm } from "react-hook-form";
import styled, { css } from 'styled-components/macro';

import { NameTitle, FormFieldTitle } from '../../Text';
import { ColorDivider, Divider } from '../../BaseStyledComponents';
import { FlexColumn } from "../../BaseStyledComponents";
import { TextAreaFormField, SingleSelectFormField } from '../../FormFields';
import { Button } from '../../Buttons';

import closeIcon from '../../../../assets/close2.svg';

const options = [
  { value: 'Male', label: 'Male' },
  { value: 'Female', label: 'Female' },
];

const EditWaitTimeModal = ({ ...props }) => {
  const {register, handleSubmit, errors} = useForm()
  const { showHandler } = props;

  const onSubmit = (data) => {
    console.log("ACCOUNT DATA", data);
  }
  return (
    <EditWaitTimeContainer>
      <ModalHeader>
        <NameTitle size='lg'>Edit wait time</NameTitle>
        <CloseIcon src={closeIcon} onClick={() => showHandler(false)} />
      </ModalHeader>
      <ColorDivider />
      <OnboardingForm onSubmit={handleSubmit(onSubmit)}>
        <InputsContainer>
          <TextAreaFormField type="text" name="summary" title="Description" placeholder="Placeholder text ..." register={register}/>
        </InputsContainer>
        <FormFieldTitle>Wait Time based on location</FormFieldTitle>
        <Divider /><br/>
        <SingleSelectFormField title="Rosny Doctors & After Hours" options={options} placeholder="Select ..." />
        <SingleSelectFormField title="Toorak Medical Centre" options={options} placeholder="Select ..." />
        <SingleSelectFormField title="Rosny Doctors & After Hours" options={options} placeholder="Select ..." />
        <SaveButton>
          <Button type="submit" size="large" radius="25" weight="sm">Save</Button>
        </SaveButton>
      </OnboardingForm>
    </EditWaitTimeContainer>
  )
}

export default EditWaitTimeModal

const EditWaitTimeContainer = styled.div`
  position: fixed;
  padding: 20px 0 0;
  top: 15vh;
  left: 0;
  right: 0;
  margin: auto;
  background: #FFFFFF;
  border-radius: 5px;
  max-width: 375px;
  z-index: 101;
  @media (max-width: 420px) {
    top: 10vh;
  }
  @media (max-width: 400px) {
    top: 6vh;
  }
  @media (max-width: 350px) {
    top: 0;
    height: 100vh;
    overflow: auto;
  }
`;

export const OnboardingForm = styled.form`
  text-align: left;
  box-sizing: content-box;
  padding: 20px;
`;

export const InputsContainer = styled(FlexColumn)`
`;

const SaveButton = styled.div`
  width: 88px;
  margin-left: auto;
  height: 30px;
`;

const ModalHeader = styled.div`
  position: relative;
  padding: 0 20px;
  margin-bottom: 16px;
`;

const CloseIcon = styled.img`
  position: absolute;
  right: 5px;
  bottom: -5px;
  cursor: pointer;
`;