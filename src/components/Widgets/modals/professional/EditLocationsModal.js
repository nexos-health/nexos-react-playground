import React, {useState, useEffect} from "react";
import { useForm, Controller } from "react-hook-form";
import styled, { css } from 'styled-components/macro';

import { NameTitle, FormFieldTitle } from '../../Text';
import { FlexColumn, ColorDivider, Divider } from "../../BaseStyledComponents";
import { TextAreaFormField, AsyncSelectFormField } from '../../FormFields';
import { Button } from '../../Buttons';
import { EditLocationCard } from '../../professional/EditLocationCard';

import closeIcon from '../../../../assets/close2.svg';

const options = [
  { value: 'one', label: 'one' },
  { value: 'two', label: 'two' },
  { value: 'three', label: 'three' },
  { value: 'four', label: 'four' },
  { value: 'five', label: 'five' },
  { value: 'six', label: 'six' },
  { value: 'seven', label: 'seven' },
  { value: 'eight', label: 'eight' },
  { value: 'nine', label: 'nine' },
];

const filterColors = (inputValue) => {
  return options.filter(i =>
    i.label.toLowerCase().includes(inputValue.toLowerCase())
  );
};

const loadOptions = (inputValue, callback) => {
  setTimeout(() => {
    callback(filterColors(inputValue));
  }, 1000);
};

const EditLocationsModal = ({ ...props }) => {
  const {register, handleSubmit, control, errors} = useForm()
  const { showHandler } = props;

  const onSubmit = (data) => {
    console.log("ACCOUNT DATA", data);
  };

  return (
    <EditLocationsContainer>
      <ModalHeader>
        <NameTitle size='lg'>Edit locations</NameTitle>
        <CloseIcon src={closeIcon} onClick={() => showHandler(false)} />
      </ModalHeader>
      <ColorDivider />
      <OnboardingForm onSubmit={handleSubmit(onSubmit)}>
        <InputsContainer>
          <Controller
            name="locations"
            control={control}
            render={({ field }) =>
              <AsyncSelectFormField
                field={field}
                title="Find a clinic"
                loadOptions={loadOptions}
                // onInputChange={onInputChange}
              />
            }
          />
        </InputsContainer>
        <AddButton>
          <Button type="submit" size="large" radius="25">Add</Button>
        </AddButton>
      </OnboardingForm>
      <LocationsContainer>
        <FormFieldTitle>Locations</FormFieldTitle>
        <Divider />
        <EditLocationCard />
        <EditLocationCard />
        <EditLocationCard />
        <SaveButton>
          <Button type="submit" size="large" radius="25" weight="sm">Save</Button>
        </SaveButton>
      </LocationsContainer>
    </EditLocationsContainer>
  )
}

export default EditLocationsModal

const EditLocationsContainer = styled.div`
  position: fixed;
  padding: 20px 0 0;
  top: 15vh;
  left: 0;
  right: 0;
  margin: auto;
  background: #FFFFFF;
  border-radius: 5px;
  max-width: 375px;
  z-index: 101;
  @media (max-width: 400px) {
    top: 13vh;
  }
  @media (max-width: 350px) {
    top: 5vh;
  }
`;

const OnboardingForm = styled.form`
  padding: 20px;
  text-align: left;
  box-sizing: content-box;
`;

const LocationsContainer = styled.div`
  text-align: left;
  box-sizing: content-box;
  padding: 0 20px 20px;
`;

const InputsContainer = styled(FlexColumn)`
`;

const SaveButton = styled.div`
  width: 88px;
  margin-left: auto;
  margin-top: 20px;
  height: 30px;
`;

const AddButton = styled.div`
  width: 88px;
  margin-left: auto;
  height: 30px;
`;

const ModalHeader = styled.div`
  position: relative;
  padding: 0 20px;
  margin-bottom: 16px;
`;

const CloseIcon = styled.img`
  position: absolute;
  right: 5px;
  bottom: -5px;
  cursor: pointer;
`;