import React, {useState, useEffect} from "react";
import { useForm, Controller } from "react-hook-form";
import styled, { css } from 'styled-components/macro';

import { NameTitle, DescriptionText, PromptText } from '../../Text';
import { FlexColumn } from "../../BaseStyledComponents";
import { SingleLineFormField, OutlineInputFormField, SingleSelectFormField } from '../../FormFields';
import { Button } from '../../Buttons';

import closeIcon from '../../../../assets/close2.svg';
import cameraIcon from '../../../../assets/camera.svg';
import logoIcon from '../../../../assets/group.png';

const options = [
  { value: 'Male', label: 'Male' },
  { value: 'Female', label: 'Female' },
];

const EditIntroModal = ({ ...props }) => {
  const {register, control, handleSubmit, errors} = useForm()
  const { showHandler, typeHandler, editPicModalHandler } = props;

  const onSubmit = (data) => {
    console.log("ACCOUNT DATA", data);
  };

  const editPicHandler = type => {
    showHandler(false);
    editPicModalHandler(true);
    typeHandler(type);
  };

  return (
    <EditIntroContainer>
      <ModalHeader>
        <NameTitle size='lg'>Edit intro</NameTitle>
        <CloseIcon src={closeIcon} onClick={() => showHandler(false)} />
      </ModalHeader>
      <PhotoContainer {...props}>
        <EditBackground onClick={() => editPicHandler('cover')}>
          <CameraIcon size="small" src={cameraIcon} />
        </EditBackground>
        <LogoContainer>
          <LogoImage src={logoIcon} />
        </LogoContainer>
        <EditLogo onClick={() => editPicHandler('profile')}>
          <CameraIcon src={cameraIcon} />
        </EditLogo>
      </PhotoContainer>
      <OnboardingForm onSubmit={handleSubmit(onSubmit)}>
        <InputsContainer>
          <OutlineInputFormField type="text" name="firstname" title="First Name" placeholder="Oliver" register={register}/>
          <OutlineInputFormField type="text" name="lastname" title="Last Name" placeholder="Jones" register={register}/>
          <OutlineInputFormField type="text" name="specialisation" title="Specialisation" placeholder="General Practice" register={register}/>
          <Controller
            name="gender"
            control={control}
            render={({ field }) =>
              <SingleSelectFormField field={field} title="Gender" options={options} placeholder="Select ..." />
            }
          />
        </InputsContainer>
        <SaveButton>
          <Button type="submit" size="large" radius="25" weight="sm">Save</Button>
        </SaveButton>
      </OnboardingForm>
    </EditIntroContainer>
  )
}

export default EditIntroModal

const EditIntroContainer = styled.div`
  position: fixed;
  padding: 20px 0 0;
  top: 15vh;
  left: 0;
  right: 0;
  margin: auto;
  background: #FFFFFF;
  border-radius: 5px;
  max-width: 375px;
  z-index: 101;
  @media (max-width: 400px) {
    top: 13vh;
  }
`;

export const OnboardingForm = styled.form`
  margin-top: 30px;
  text-align: left;
  box-sizing: content-box;
  padding: 20px;
`;

export const InputsContainer = styled(FlexColumn)`
`;

const LogoContainer = styled.div`
  position: absolute;
  top: 32px;
  left: 20px;
  width: 100px;
  height: 100px;
  text-align: center;
  background: #69B9FF;
  border-radius: 100px;
  border: 2px solid #FFFFFF;
`;

const EditBackground = styled.div`
  position: absolute;
  top: 4px;
  right: 8px;
  border: 0.5px solid #CCCCCC;
  box-sizing: border-box;
  width: 28px;
  height: 28px;
  background: #FFFFFF;
  border-radius: 40px;
  text-align: center;
  cursor: pointer;
`;

const SaveButton = styled.div`
  width: 88px;
  margin-left: auto;
  height: 30px;
`;

const EditLogo = styled.div`
  position: absolute;
  top: 102px;
  left: 90px;
  border: 0.5px solid #CCCCCC;
  box-sizing: border-box;
  width: 30px;
  height: 30px;
  background: #FFFFFF;
  border-radius: 40px;
  text-align: center;
  cursor: pointer;
`;

const PhotoContainer = styled.div`
  position: relative;
  height: 98px;
  ${props => props.backgroundImg ? css`
    background: url(${props.backgroundImg});
    background-repeat: no-repeat;
    background-size: cover;
  ` : css`
    background: #B1DBFF;
  `};
`;

const LogoImage = styled.img`
  margin-top: 15px;
`;

const ModalHeader = styled.div`
  position: relative;
  padding: 0 20px;
  margin-bottom: 16px;
`;

const CloseIcon = styled.img`
  position: absolute;
  right: 5px;
  bottom: -5px;
  cursor: pointer;
`;

const CameraIcon = styled.img`
  ${props => props.size === "small" ? css`
    width: 12px;
    height: 10px;
    margin-bottom: 2px;
  ` : css`
    width: 14px;
    height: 12px;
  `};
`;