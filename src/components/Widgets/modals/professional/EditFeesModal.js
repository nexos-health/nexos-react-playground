import React from "react";
import { useForm } from "react-hook-form";
import styled, { css } from 'styled-components/macro';

import { NameTitle } from '../../Text';
import { FlexColumn } from "../../BaseStyledComponents";
import { TextAreaFormField } from '../../FormFields';
import { Button } from '../../Buttons';
import { ColorDivider } from '../../BaseStyledComponents';
import { EditFeeCard } from '../../professional/EditFeeCard';

import closeIcon from '../../../../assets/close2.svg';

const EditFeesModal = ({ ...props }) => {
  const {register, handleSubmit, errors} = useForm()
  const { showHandler } = props;

  const onSubmit = (data) => {
    console.log("ACCOUNT DATA", data);
  }

  return (
    <EditFeesContainer>
      <ModalHeader>
        <NameTitle size='lg'>Edit Fees</NameTitle>
        <CloseIcon src={closeIcon} onClick={() => showHandler(false)} />
      </ModalHeader>
      <ColorDivider />
      <OnboardingForm onSubmit={handleSubmit(onSubmit)}>
        <InputsContainer>
          <TextAreaFormField type="text" name="description" title="Description" placeholder="Placeholder text ..." register={register}/>
        </InputsContainer>
        <div>
          <EditFeeCard />
          <EditFeeCard />
        </div>
        <SaveButton>
          <Button type="submit" size="large" radius="25" weight="sm">Save</Button>
        </SaveButton>
      </OnboardingForm>
    </EditFeesContainer>
  )
}

export default EditFeesModal

const EditFeesContainer = styled.div`
  position: fixed;
  padding: 20px 0 0;
  top: 15vh;
  left: 0;
  right: 0;
  margin: auto;
  background: #FFFFFF;
  border-radius: 5px;
  max-width: 375px;
  z-index: 101;
  @media (max-width: 420px) {
    top: 10vh;
  }
  @media (max-width: 400px) {
    top: 4vh;
  }
  @media (max-width: 350px) {
    top: 0;
    height: 100vh;
    overflow: auto;
  }
`;

export const OnboardingForm = styled.form`
  text-align: left;
  box-sizing: content-box;
  padding: 20px;
`;

export const InputsContainer = styled(FlexColumn)`
`;

const ActionContainer = styled.div`
  position: relative;
  display: flex;
  padding: 10px 20px;
`;

const SaveButton = styled.div`
  width: 88px;
  margin-left: auto;
  margin-top: 20px;
  height: 30px;
`;

const ModalHeader = styled.div`
  position: relative;
  padding: 0 20px;
  margin-bottom: 16px;
`;

const CloseIcon = styled.img`
  position: absolute;
  right: 5px;
  bottom: -5px;
  cursor: pointer;
`;