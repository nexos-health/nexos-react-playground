export { default as EditAdditionalInfoModal} from './EditAdditionalInfoModal';
export { default as EditFeesModal } from './EditFeesModal';
export { default as CreateNewClinicModal } from './CreateNewClinicModal';
export { default as EditIntroModal } from './EditIntroModal';
export { default as EditWaitTimeModal } from './EditWaitTimeModal';
export { default as EditLocationsModal } from './EditLocationsModal';