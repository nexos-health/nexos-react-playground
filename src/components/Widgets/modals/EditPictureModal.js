import React, {useState, useEffect} from "react";
import styled, { css } from 'styled-components/macro';

import { NameTitle, DescriptionText, PromptText } from '../Text';
import { ColorDivider } from '../BaseStyledComponents';

import closeIcon from '../../../assets/close2.svg';
import editIcon from '../../../assets/edit.svg';
import deleteIcon from '../../../assets/delete.svg';
import coverImg from '../../../assets/coverpic.png';
import profileImg from '../../../assets/avatar2.png';

const EditPictureModal = ({ ...props }) => {
  const { type, showHandler } = props;

  return (
    <EditPictureContainer type={type}>
      <ModalHeader>
        <NameTitle>{type === 'profile' ? 'Profile picture' : 'Cover picture'}</NameTitle>
        <CloseIcon src={closeIcon} onClick={() => showHandler(false)} />
      </ModalHeader>
      <ColorDivider />
      <ImageContainer>
        <ImageView src={type === 'cover' ? coverImg : profileImg} type={type} />
      </ImageContainer>
      <ColorDivider />
      <ActionContainer>
        <Action>
          <ActionIcon src={deleteIcon} />
          <DescriptionText>Delete</DescriptionText>
        </Action>
        <Action>
          <ActionIcon src={editIcon} />
          <DescriptionText>Edit</DescriptionText>
        </Action>
        <SaveButton>
          <PromptText color='white'>Save</PromptText>
        </SaveButton>
      </ActionContainer>
    </EditPictureContainer>
  )
}

export default EditPictureModal

const EditPictureContainer = styled.div`
  position: fixed;
  padding: 20px 0;
  top: 15vh;
  left: 0;
  right: 0;
  margin: auto;
  background: #FFFFFF;
  border-radius: 5px;
  max-width: 375px;
  ${props => props.type === 'cover' ? css`
    height: 374px;
  ` : css`
    height: 492px;
  `};
  z-index: 101;
  @media (max-width: 400px) {
    top: 13vh;
  }
  @media (max-width: 350px) {
    top: 10vh;
  }
`;

const ImageContainer = styled.div`
  padding: 20px;
`;

const ActionContainer = styled.div`
  position: relative;
  display: flex;
  padding: 10px 20px;
`;

const Action = styled.div`
  text-align: center;
  margin-right: 20px;
  cursor: pointer;
`;

const SaveButton = styled.div`
  position: absolute;
  top: 15px;
  right: 20px;
  padding: 4px 20px;
  height: 30px;
  width: fit-content;
  background: #2F80ED;
  border-radius: 25px;
  cursor: pointer;
`;

const ModalHeader = styled.div`
  position: relative;
  padding: 0 20px;
  margin-bottom: 16px;
`;

const CloseIcon = styled.img`
  position: absolute;
  right: 5px;
  bottom: -5px;
  cursor: pointer;
`;

const ImageView = styled.img`
  width: 100%;
  max-width: 335px;
  ${props => props.type === 'cover' ? css`
    height: 200px;
  ` : css`
    height: 318px;
  `};
`;

const ActionIcon = styled.img`
`;