import React from "react";
import { useForm } from "react-hook-form";
import styled, { css } from 'styled-components/macro';

import { NameTitle, FormFieldTitle } from '../../Text';
import { ColorDivider, Divider } from '../../BaseStyledComponents';
import { FlexColumn } from "../../BaseStyledComponents";
import { TextAreaFormField } from '../../FormFields';
import { Button } from '../../Buttons';

import closeIcon from '../../../../assets/close2.svg';
import imagePlusIcon from '../../../../assets/imageplus.svg';
import deleteIcon from '../../../../assets/delete.svg';
import example from '../../../../assets/avatar2.png';

const EditAboutModal = ({ ...props }) => {
  const {register, handleSubmit, errors} = useForm()
  const { showHandler } = props;

  const onSubmit = (data) => {
    console.log("ACCOUNT DATA", data);
  }
  return (
    <EditAboutContainer>
      <ModalHeader>
        <NameTitle size='lg'>Edit about</NameTitle>
        <CloseIcon src={closeIcon} onClick={() => showHandler(false)} />
      </ModalHeader>
      <ColorDivider />
      <OnboardingForm onSubmit={handleSubmit(onSubmit)}>
        <InputsContainer>
          <TextAreaFormField type="text" name="summary" title="Summary" placeholder="Placeholder text ..." register={register}/>
        </InputsContainer>
        <FormFieldTitle>Gallery</FormFieldTitle>
        <Divider />
        <ImagesContainer>
          <ImageContainer>
            <Photo src={example}></Photo>
            <PhotoDeleteBtn src={deleteIcon} />
          </ImageContainer>
          <ImageContainer>
            <Photo src={example}></Photo>
            <PhotoDeleteBtn src={deleteIcon} />
          </ImageContainer>
          <ImageContainer>
            <Photo src={example}></Photo>
            <PhotoDeleteBtn src={deleteIcon} />
          </ImageContainer>
          <ImageContainer empty>
            <PhotoPlus src={imagePlusIcon}></PhotoPlus>
          </ImageContainer>
        </ImagesContainer>
        <SaveButton>
          <Button type="submit" size="large" radius="25" weight="sm">Save</Button>
        </SaveButton>
      </OnboardingForm>
    </EditAboutContainer>
  )
}

export default EditAboutModal

const EditAboutContainer = styled.div`
  position: fixed;
  padding: 20px 0 0;
  top: 15vh;
  left: 0;
  right: 0;
  margin: auto;
  background: #FFFFFF;
  border-radius: 5px;
  max-width: 375px;
  z-index: 101;
  @media (max-width: 420px) {
    top: 5vh;
  }
  @media (max-width: 400px) {
    top: 0;
    height: 100vh;
    overflow: auto;
  }
`;

const ImagesContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 15px;
`;

const ImageContainer = styled.div`
  position: relative;
  ${props => props.empty && css`
    background: #B1DBFF;
  `}
`;

export const OnboardingForm = styled.form`
  text-align: left;
  box-sizing: content-box;
  padding: 20px;
`;

export const InputsContainer = styled(FlexColumn)`
`;

const SaveButton = styled.div`
  width: 88px;
  margin-left: auto;
  margin-top: 20px;
  height: 30px;
`;

const ModalHeader = styled.div`
  position: relative;
  padding: 0 20px;
  margin-bottom: 16px;
`;

const CloseIcon = styled.img`
  position: absolute;
  right: 5px;
  bottom: -5px;
  cursor: pointer;
`;

const Photo = styled.img`
  width: 100%;
  height: 100%;
`;

const PhotoPlus = styled.img`
  margin: auto;
  position: absolute;
  top: 0; left: 0; bottom: 0; right: 0;
  cursor: pointer;
`;

const PhotoDeleteBtn = styled.img`
  position: absolute;
  top: 5px;
  right: 5px;
  width: 30px;
`;