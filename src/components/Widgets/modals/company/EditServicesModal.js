import React, {useState, useEffect} from "react";
import { useForm, Controller } from "react-hook-form";
import styled, { css } from 'styled-components/macro';

import { NameTitle, FormFieldTitle } from '../../Text';
import { FlexColumn, ColorDivider } from "../../BaseStyledComponents";
import { TextAreaFormField, MultiSelectFormField, CheckBoxFormField } from '../../FormFields';
import { Button } from '../../Buttons';

import closeIcon from '../../../../assets/close2.svg';

const options = [
  { value: 'one', label: 'one' },
  { value: 'two', label: 'two' },
  { value: 'three', label: 'three' },
  { value: 'four', label: 'four' },
  { value: 'five', label: 'five' },
  { value: 'six', label: 'six' },
  { value: 'seven', label: 'seven' },
  { value: 'eight', label: 'eight' },
  { value: 'nine', label: 'nine' },
];

const EditServicesModal = ({ ...props }) => {
  const {register, handleSubmit, control, errors} = useForm()
  const { showHandler } = props;

  const onSubmit = (data) => {
    console.log("ACCOUNT DATA", data);
  };

  return (
    <EditServicesContainer>
      <ModalHeader>
        <NameTitle size='lg'>Edit services</NameTitle>
        <CloseIcon src={closeIcon} onClick={() => showHandler(false)} />
      </ModalHeader>
      <ColorDivider />
      <OnboardingForm onSubmit={handleSubmit(onSubmit)}>
        <InputsContainer>
          <Controller
            name="services"
            control={control}
            render={({ field }) =>
              <MultiSelectFormField
                field={field}
                options={options}
                title="Field of specialization"
              />
            }
          />
          <TextAreaFormField type="text" name="description" title="Description of services" placeholder="Placeholder text ..." register={register}/>
        </InputsContainer>
        <FormFieldTitle>Extra service</FormFieldTitle>
        <ExtraContainer>
          <CheckBoxFormField underline name="telehealth" title="Telehealth" register={register} />
          <CheckBoxFormField name="bulk" title="Bullk billing" register={register} />
          <CheckBoxFormField name="urgent" title="Urgent appointments" register={register} />
          <CheckBoxFormField name="telehealth2" title="Telehealth" register={register} />
        </ExtraContainer>
        <SaveButton>
          <Button type="submit" size="large" radius="25" weight="sm">Save</Button>
        </SaveButton>
      </OnboardingForm>
    </EditServicesContainer>
  )
}

export default EditServicesModal

const EditServicesContainer = styled.div`
  position: fixed;
  padding: 20px 0 0;
  top: 15vh;
  left: 0;
  right: 0;
  margin: auto;
  background: #FFFFFF;
  border-radius: 5px;
  max-width: 375px;
  z-index: 101;
  @media (max-width: 420px) {
    top: 7vh;
  }
  @media (max-width: 400px) {
    top: 0;
    height: 100vh;
    overflow: auto;
  }
`;

const OnboardingForm = styled.form`
  text-align: left;
  box-sizing: content-box;
  padding: 20px;
`;

const InputsContainer = styled(FlexColumn)`
`;

const ExtraContainer = styled.div`
`;

const ActionContainer = styled.div`
  position: relative;
  display: flex;
  padding: 10px 20px;
`;

const SaveButton = styled.div`
  width: 88px;
  margin-left: auto;
  margin-top: 20px;
  height: 30px;
`;

const ModalHeader = styled.div`
  position: relative;
  padding: 0 20px;
  margin-bottom: 16px;
`;

const CloseIcon = styled.img`
  position: absolute;
  right: 5px;
  bottom: -5px;
  cursor: pointer;
`;