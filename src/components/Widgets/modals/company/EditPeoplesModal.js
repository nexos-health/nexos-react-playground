import React, {useState, useEffect} from "react";
import { useForm, Controller } from "react-hook-form";
import styled, { css } from 'styled-components/macro';

import { NameTitle, FormFieldTitle } from '../../Text';
import { FlexColumn, ColorDivider, Divider } from "../../BaseStyledComponents";
import { TextAreaFormField, AsyncSelectFormField } from '../../FormFields';
import { Button } from '../../Buttons';
import { EditPeopleCard } from '../../company/EditPeopleCard';

import closeIcon from '../../../../assets/close2.svg';

const options = [
  { value: 'James', label: 'James' },
  { value: 'Jones', label: 'Jones' },
  { value: 'Smith', label: 'Smith' },
  { value: 'Susan', label: 'Susan' },
];

const filterColors = (inputValue) => {
  return options.filter(i =>
    i.label.toLowerCase().includes(inputValue.toLowerCase())
  );
};

const loadOptions = (inputValue, callback) => {
  setTimeout(() => {
    callback(filterColors(inputValue));
  }, 1000);
};

const EditPeoplesModal = ({ ...props }) => {
  const {register, handleSubmit, control, errors} = useForm()
  const { showHandler } = props;

  const onSubmit = (data) => {
    console.log("ACCOUNT DATA", data);
  };

  return (
    <EditPeoplesContainer>
      <ModalHeader>
        <NameTitle size='lg'>Edit people</NameTitle>
        <CloseIcon src={closeIcon} onClick={() => showHandler(false)} />
      </ModalHeader>
      <ColorDivider />
      <OnboardingForm onSubmit={handleSubmit(onSubmit)}>
        <InputsContainer>
          <TextAreaFormField type="text" name="description" title="Description of the people" placeholder="Placeholder text ..." register={register}/>
          <Controller
            name="locations"
            control={control}
            render={({ field }) =>
              <AsyncSelectFormField
                field={field}
                title="Add practitioner"
                loadOptions={loadOptions}
                // onInputChange={onInputChange}
              />
            }
          />
        </InputsContainer>
        <AddButton>
          <Button type="submit" size="large" radius="25" weight="sm">Add</Button>
        </AddButton>
      </OnboardingForm>
      <PeoplesContainer>
        <GroupTitle>General Practicioners</GroupTitle>
        <EditPeopleCard />
        <EditPeopleCard />
        <EditPeopleCard />
        <EditPeopleCard />
        <GroupTitle>Psychologist</GroupTitle>
        <EditPeopleCard />
        <EditPeopleCard />
        <EditPeopleCard />
        <EditPeopleCard />
      </PeoplesContainer>
      <SaveButton>
        <Button type="submit" size="large" radius="25" weight="sm">Save</Button>
      </SaveButton>
    </EditPeoplesContainer>
  )
}

export default EditPeoplesModal

const EditPeoplesContainer = styled.div`
  position: fixed;
  padding: 20px 0 20px;
  top: 15vh;
  left: 0;
  right: 0;
  margin: auto;
  background: #FFFFFF;
  border-radius: 5px;
  max-width: 375px;
  z-index: 101;
  @media (max-width: 420px) {
    top: 5vh;
  }
  @media (max-width: 420px) {
    top: 0;
    height: 100vh;
    overflow: auto;
  }
`;

const OnboardingForm = styled.form`
  padding: 20px;
  text-align: left;
  box-sizing: content-box;
`;

const PeoplesContainer = styled.div`
  text-align: left;
  box-sizing: content-box;
  padding: 0 20px;
  height: 240px;
  overflow: auto;
`;

const InputsContainer = styled(FlexColumn)`
`;

const SubmitContainer = styled.div`
  position: relative;
  display: flex;
  padding: 18px 20px;
`;

const SaveButton = styled.div`
  margin-top: 20px;
  margin-left: auto;
  margin-right: 20px;
  width: 88px;
  height: 30px;
`;

const AddButton = styled.div`
  width: 88px;
  margin-left: auto;
  height: 30px;
`;

const ModalHeader = styled.div`
  position: relative;
  padding: 0 20px;
  margin-bottom: 16px;
`;

const CloseIcon = styled.img`
  position: absolute;
  right: 5px;
  bottom: -5px;
  cursor: pointer;
`;

const GroupTitle = styled(FormFieldTitle)`
  margin-top: 15px;
`;