import React, {useState, useEffect} from "react";
import { useForm } from "react-hook-form";
import styled, { css } from 'styled-components/macro';

import { NameTitle } from '../../Text';
import { FlexColumn } from "../../BaseStyledComponents";
import { TimeScheduleFormField } from '../../FormFields';
import { Button } from '../../Buttons';
import { ColorDivider, Divider } from '../../BaseStyledComponents';

import closeIcon from '../../../../assets/close2.svg';

const EditScheduleModal = ({ ...props }) => {
  const {register, handleSubmit, errors} = useForm()
  const { showHandler } = props;

  const onSubmit = (data) => {
    console.log("ACCOUNT DATA", data);
  }

  return (
    <EditIntroContainer>
      <ModalHeader>
        <NameTitle size='lg'>Edit opening hours</NameTitle>
        <CloseIcon src={closeIcon} onClick={() => showHandler(false)} />
      </ModalHeader>
      <ColorDivider />
      <MainContainer>
        <FieldName>
          <NameTitle size="md">Day of the week</NameTitle>
          <TimeField>
            <NameTitle size="md">Start Time</NameTitle>&nbsp;&nbsp;
            <NameTitle size="md">End Time</NameTitle>
          </TimeField>
        </FieldName>
        <Divider />
        <OnboardingForm onSubmit={handleSubmit(onSubmit)}>
          <InputsContainer>
            <TimeScheduleFormField defaultValue type="text" name="mon" title="Manday" register={register}/>
            <TimeScheduleFormField defaultValue type="text" name="tue" title="Tuesday" register={register}/>
            <TimeScheduleFormField defaultValue type="text" name="wed" title="Wednesday" register={register}/>
            <TimeScheduleFormField defaultValue type="text" name="thu" title="Thursday" register={register}/>
            <TimeScheduleFormField defaultValue type="text" name="fri" title="Friday" register={register}/>
            <TimeScheduleFormField type="text" name="sat" title="Saturday" register={register}/>
            <TimeScheduleFormField type="text" name="sun" title="Sunday" register={register}/>
          </InputsContainer>
          <Divider />
          <SaveButton>
            <Button type="submit" size="large" radius="25" weight="sm">Save</Button>
          </SaveButton>
        </OnboardingForm>
      </MainContainer>
    </EditIntroContainer>
  )
}

export default EditScheduleModal

const EditIntroContainer = styled.div`
  position: fixed;
  padding: 20px 0 0;
  top: 15vh;
  left: 0;
  right: 0;
  margin: auto;
  background: #FFFFFF;
  border-radius: 5px;
  max-width: 420px;
  z-index: 101;
  @media (max-width: 400px) {
    top: 13vh;
  }
  @media (max-width: 350px) {
    top: 2vh;
  }
`;

const MainContainer = styled.div`
  margin-top: 15px;
  padding: 0 20px;
`;

export const OnboardingForm = styled.form`
  text-align: left;
  box-sizing: content-box;
  padding: 20px 0;
`;

export const InputsContainer = styled(FlexColumn)`
`;

const SaveButton = styled.div`
  width: 88px;
  margin-left: auto;
  margin-top: 20px;
  height: 30px;
`;

const FieldName = styled.div`
  position: relative;
  display: flex;
`;

const TimeField = styled.div`
  position: absolute;
  display: flex;
  right: 0;
`;

const ModalHeader = styled.div`
  position: relative;
  padding: 0 20px;
  margin-bottom: 16px;
`;

const CloseIcon = styled.img`
  position: absolute;
  right: 5px;
  bottom: -5px;
  cursor: pointer;
`;