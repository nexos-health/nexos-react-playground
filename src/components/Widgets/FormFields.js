import React from "react";
import styled, { css } from "styled-components";
import {FormFieldTitle} from "./Text";
import {FlexColumn} from "./BaseStyledComponents";
import {SingleLineInput, TextArea, TimeInput, CheckBox, OutlineInput} from "./Inputs";
import {MultiSelect, SingleSelect, StyledAsyncSelect} from './Selects';

export const SingleLineFormField = ({title, defaultValue="", placeholder="", name="", register}) => {
  return (
    <StyledFormContainer>
      <FormFieldTitle>{title}</FormFieldTitle>
      <SingleLineInput default={defaultValue} placeholder={placeholder} name={name} register={register}/>
    </StyledFormContainer>
  )
}

export const TextAreaFormField = ({title, size="", defaultValue="", placeholder="", name="", register}) => {
  return (
    <StyledFormContainer>
      <FormFieldTitle>{title}</FormFieldTitle>
      <TextArea size={size} default={defaultValue} placeholder={placeholder} name={name} register={register} />
    </StyledFormContainer>
  )
}

export const TimeScheduleFormField = ({title, defaultValue=false, name="", register}) => {
  return (
    <StyledFormContainer row>
      <TimeFormFieldTitle>{title}</TimeFormFieldTitle>
      <TimeContainer>
        <ScheduleTimeInput defaultValue={defaultValue && "08:00"} name={`${name}_start`} register={register} />
        <ScheduleTimeInput defaultValue={defaultValue && "17:00"} name={`${name}_end`} register={register} />
      </TimeContainer>
    </StyledFormContainer>
  )
}

export const OutlineInputFormField = ({title, defaultValue="", placeholder="", name="", register}) => {
  return (
    <StyledFormContainer row>
      <NonLineTitleContainer>
        <TimeFormFieldTitle>{title}</TimeFormFieldTitle>
      </NonLineTitleContainer>
      <OutlineInput default={defaultValue} placeholder={placeholder} name={name} register={register} />
    </StyledFormContainer>
  )
}

export const CheckBoxFormField=({title, underline, defaultValue=false, name="", register}) => {
  return (
    <StyledFormContainer row underline>
      <TimeFormFieldTitle>{title}</TimeFormFieldTitle>
      <TimeContainer>
        <CheckBox default={defaultValue} name={name} register={register} />
      </TimeContainer>
    </StyledFormContainer>
  )
}

export const MultiSelectFormField = ({ field, title, options, value, inputValue, onChange, onInputChange, placeholder, register}) => {
  return (
    <StyledFormContainer>
      <FormFieldTitle>{title}</FormFieldTitle>
      <MultiSelect
        field={field}
        options={options}
        value={value}
        inputValue={inputValue}
        onChange={onChange}
        onInputChange={onInputChange}
        register={register}
      />
    </StyledFormContainer>
  )
}

export const SingleSelectFormField = ({ field, title, options, value, placeholder, onChange, register }) => {
  return (
    <StyledFormContainer>
      <FormFieldTitle>{title}</FormFieldTitle>
      <SingleSelect
        field={field}
        placeholder={placeholder}
        options={options}
        value={value}
        onChange={onChange}
        register={register}
      />
    </StyledFormContainer>
  )
}

export const AsyncSelectFormField = ({ field, title, loadOptions, onInputChange, onChange, placeholder }) => {
  return (
    <StyledFormContainer>
      <FormFieldTitle>{title}</FormFieldTitle>
      <StyledAsyncSelect
        field={field}
        placeholder={placeholder}
        loadOptions={loadOptions}
        onChange={onChange}
        onInputChange={onInputChange}
      />
    </StyledFormContainer>
  )
}

const StyledFormContainer = styled(FlexColumn)`
  position: relative;
  ${props => props.row && css`
    display: flex;
    flex-direction: row;
  `};
  padding-bottom: 20px;
  ${props => props.underline && css`
    border-bottom: 1px solid #E2E9F3;
    padding: 12px 0 10px;
  `}
`;

const TimeContainer = styled.div`
  position: absolute;
  right: 0;
`;

const TimeFormFieldTitle = styled(FormFieldTitle)`
  margin-top: 5px;
`;

const ScheduleTimeInput = styled(TimeInput)`
  margin-left: 17px;
`;

const NonLineTitleContainer = styled.div`
  position: absolute;
  left: 0;
`;
