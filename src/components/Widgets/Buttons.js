import React from "react";
import styled, {css} from "styled-components/macro";
import {DARK_TEXT_COLOR, PRIMARY_COLOR} from "../../utils/constants";
import {HashLink} from "react-router-hash-link";


// Rectangular Buttons

const StyledButton = styled.button`
  font-size: ${props => props.fontSize === 'sm' ? '14px' : '16px'};
  font-weight: ${props => props.weight === 'sm' ? 'normal' : '700'};
  border-radius: ${props => `${props.radius}px`};
  border: none;
  padding: 3px 15px;
  white-space: nowrap;
  text-align: center;
  cursor: pointer;
  text-decoration: ${props => props.underline};
  ${props => props.variant === "secondary" ? css`
    background-color: white;
    color: ${DARK_TEXT_COLOR};
  ` : props.variant === "link" ? css`
    background-color: white;
    color: ${PRIMARY_COLOR};
    box-shadow: none;
    padding: 0;
  ` : props.variant === "outline-primary" ? css`
    background-color: white;
    color: ${PRIMARY_COLOR};
    border: 1px solid #3FA2F7;
    box-shadow: none;
  ` : css`
    background-color: ${PRIMARY_COLOR};
    color: white;
  `};
  ${props => props.size === "large" && css`
    width: 100%;
    padding: 5px 15px;
  `};
  ${props => props.shape === "rounded" && css`
    border-radius: 20px;
  `};
  ${props => props.float === "right" && css`
    float: right;
  `};
  &:focus {
    outline: none;
  }

  @media (min-width: 600px) {
    max-width: 400px;
  }
`

export const Button = ({children, weight="", size="small", fontSize="", shape="rectangle", variant="primary", underline="none", float="left", radius="3", onClick}) => {
  return (
    <StyledButton size={size} shape={shape} variant={variant} fontSize={fontSize} float={float} weight={weight} underline={underline} radius={radius} onClick={onClick}>
      {children}
    </StyledButton>
  )
}



// Homepage Buttons

const buttonStyles = `
  width: 100%;
  height: 50px;
  background-color: ${PRIMARY_COLOR};
  color: white;
  font-size: 14px;
  font-weight: 700;
  border-radius: 5px;
  border: none;
  padding: 15px 25px;
  box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.25);
  white-space: nowrap;
  text-align: center;
  cursor: pointer;
  text-decoration: none;
  margin-bottom: 10px;
  ${props => props.secondary && css`
    background-color: white;
    color: #000104;
  `}
  ${props => props.size === "small" && css`
    font-size: 12px;
    height: 20px;
  `}
  
  @media (min-width: 600px) {
    max-width: 175px;
  }
`

export const HomePageButton = ({
  children,
  className,
  icon=null,
  type,
  onClick,
  style,
  size="large",
  link=false,
  to
}) => {
  return (
    <>
      {link
        ? <StyledLinkButton className={className} size={size} to={to} onClick={onClick} type={type}>
          {icon}
          {children}
        </StyledLinkButton>
        : <StyledFancyButton className={className} size={size} onClick={onClick} type={type}>
          {icon}
          {children}
        </StyledFancyButton>
      }
    </>
  )
}


const StyledFancyButton = styled.button`
  ${buttonStyles}
`;

const StyledLinkButton = styled(HashLink)`
  ${buttonStyles}
`;